<?php

namespace App\Http\Controllers\Resource;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Setting;
use Exception;
use App\Helpers\Helper;

use App\CarClass;

class CarClassResource extends Controller{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request){
        $CarClasses = CarClass::all();
        if($request->ajax()) {
            return $CarClasses;
        } else {
            return view('admin.carclass.index', compact('CarClasses'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        $Car_Class = $request->all();
        if($request->id){
            $this->validate($request, [
                'name' => "required|max:255|unique:car_classes,name,{$request->id}",
                'logo' => 'mimes:jpeg,png,icon'
            ]);
            try {
                $CarClass = CarClass::findOrFail($request->id);
                if($request->hasFile('logo')) {
                    if($CarClass->logo) { Helper::delete_picture($CarClass->logo);}
                    $Car_Class['logo'] = Helper::upload_picture($request->logo);
                }
                $CarClass->update($Car_Class);
                return back()->with('flash_success','Car Class Saved Successfully');
            } catch (Exception $e) {
                return back()->with('flash_error', 'Car Class Not Valid');
            }
        }else{
            $this->validate($request, [
                'name' => 'required|max:255|unique:car_classes,name',
                'logo' => 'required|mimes:jpeg,png,icon'
            ]);
            try {
                $Car_Class['logo'] = Helper::upload_picture($request->logo);
                $CarClass = CarClass::create($Car_Class);
                return back()->with('flash_success','Car Class Saved Successfully');
            } catch (Exception $e) {
                return back()->with('flash_error', 'Car Class Not Valid');
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ServiceType  $serviceType
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            return CarClass::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            return back()->with('flash_error', 'Car Class Not Found');
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ServiceType  $serviceType
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){        
        try {
            $CarClass = CarClass::findOrFail($id);
            Helper::delete_picture($CarClass->logo);
            $CarClass->delete();
            return back()->with('flash_success', 'Car Class deleted successfully');
        } catch (ModelNotFoundException $e) {
            return back()->with('flash_error', 'Car Class Not Found');
        }
    }
    public function CarClasses(){
        return CarClass::where(['status' => 1])->get();
    }
}