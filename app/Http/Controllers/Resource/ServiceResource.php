<?php

namespace App\Http\Controllers\Resource;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Setting;
use Exception;
use App\Helpers\Helper;

use App\ServiceType;
use App\TransportationType;

class ServiceResource extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $services = ServiceType::all();
        if($request->ajax()) {
            return $services;

        } else {
            return view('admin.service.index', compact('services'));
        }
    }
    public function changeStatus(Request $request)
    {
        $servicess = ServiceType::all();
        $services = ServiceType::findOrFail($_POST['service_id']);
        if($request->ajax()) {
            $services->status=$_POST['status'];
            $services->save();
            return $services;
        } else {
            return view('admin.service.index', compact('servicess'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.service.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        $Service = $request->all();
        if($request->id){
            $this->validate($request, [
                'name' => "required|max:255|unique:service_types,name,{$request->id}",
                'image' => 'mimes:jpeg,png,icon',
                'description' => 'required|max:255',
                'fixed' => 'required|numeric',
                'price' => 'required|numeric',
                'minute' => 'required|numeric',
                'distance' => 'required|numeric',
                'calculator' => 'required|in:MIN,HOUR,DISTANCE,DISTANCEMIN,DISTANCEHOUR'
            ]);

            try {
                $ServiceType = ServiceType::findOrFail($request->id);
                if($request->hasFile('image')) {
                    Helper::delete_picture($ServiceType->image);
                    $Service['image'] = Helper::upload_picture($request->image);
                }
                $ServiceType->update($Service);
                return redirect()->route('admin.service.index')->with('flash_success', 'Service Type Updated Successfully');
            } catch (Exception $e) {
                return back()->with('flash_error', 'Transportation Type Not Found');
            }
        }else{
            $this->validate($request, [
                'name' => 'required|max:255|unique:service_types,name',
                'image' => 'required|mimes:jpeg,png,icon',
                'description' => 'required|max:255',
                'fixed' => 'required|numeric',
                'price' => 'required|numeric',
                'minute' => 'required|numeric',
                'distance' => 'required|numeric',
                'calculator' => 'required|in:MIN,HOUR,DISTANCE,DISTANCEMIN,DISTANCEHOUR'
            ]);
            try {
                $Service['image'] = Helper::upload_picture($request->image);
                $ServiceType = ServiceType::create($Service);
                return redirect()->route('admin.service.index')->with('flash_success', 'Service Type Updated Successfully');
            } catch (ModelNotFoundException $e) {
                return back()->with('flash_error', 'Service Type Not Found');
            }
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ServiceType  $serviceType
     * @return \Illuminate\Http\Response
     */
    public function show($id){
        try {
            return ServiceType::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            return back()->with('flash_error', 'Service Type Not Found');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ServiceType  $serviceType
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $service = ServiceType::findOrFail($id);
            return view('admin.service.edit',compact('service'));
        } catch (ModelNotFoundException $e) {
            return back()->with('flash_error', 'Service Type Not Found');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ServiceType  $serviceType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request){

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ServiceType  $serviceType
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){        
        try{
            $ServiceType = ServiceType::findOrFail($id);
            Helper::delete_picture($ServiceType->image);
            $ServiceType->delete();
            return back()->with('message', 'Service Type deleted successfully');
        } catch (ModelNotFoundException $e) {
            return back()->with('flash_error', 'Service Type Not Found');
        }
    }
    public function services(){
        return ServiceType::where(['status' => 1])->get();
    }





//
//
    public function frontend(Request $request)
    {
        $services = ServiceType::findOrFail($request->id);
        return view('public.services.index', compact('services'));

    }

}
