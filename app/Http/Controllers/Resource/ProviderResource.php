<?php

namespace App\Http\Controllers\Resource;

use App\CarClass;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;

use DB;
use Exception;
use Setting;
use Storage;

use App\Provider;
use App\ProviderService;
use App\UserRequestPayment;
use App\UserRequests;
use App\Helpers\Helper;


class ProviderResource extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $AllProviders = Provider::with('service','accepted','cancelled')
                    ->orderBy('id', 'DESC');

        if(request()->has('fleet')){
            $providers = $AllProviders->where('fleet',$request->fleet)->get();
        }else{
            $providers = $AllProviders->get();
        }
        return view('admin.providers.index', compact('providers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     *  @return Provider
     */
    public function create()
    {
        $fleet=DB::table('fleets')->get();
        $service_types=DB::table('service_types')->get();
        return view('admin.providers.create',compact('fleet','service_types'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param $Provider
     * @param $providerservice
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(Setting::get('demo_mode', 0) == 1) {
            return back()->with('flash_error', 'Disabled for demo purposes! Please contact us at info@appoets.com');
        }

        $this->validate($request, [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'email' => 'required|unique:providers,email|email|max:255',
            'mobile' => 'required|between:11,13',
            'avatar' => 'mimes:jpeg,jpg,bmp,png|max:5242880',
            'password' => 'required|min:6|confirmed',
            'fleet' => 'required',
            'service_type' => 'required',
            'service_number' => 'required|min:6|max:8',
            'service_model' => 'required',
        ]);

        try{

            $provider = $request->all();
            $provider['password'] = bcrypt($request->password);
            $provider['fleet'] = $request->fleet;

            if($request->hasFile('avatar')) {
                $provider['avatar'] = $request->avatar->store('uploads');
            }
            $provider = Provider::create($provider);

            $this->saveProviderImages($provider, $request);
            $provider->serviceTypes()->attach($request->service_type);


            return back()->with('flash_success','Provider Details Saved Successfully');
        }
        catch (Exception $e) {
            return back()->with('flash_error', 'Provider Not Found');
        }

    }


    private function saveProviderImages($Provider, $request) {
        $pro = Provider::find($Provider->id);

        if(! empty($request->car_front)) $pro->car_front                        = Helper::upload_picture($request->file('car_front'));
        if(! empty($request->logo)) $pro->logo                                  = Helper::upload_picture($request->file('logo'));
        if(! empty($request->car_back)) $pro->car_back                          = Helper::upload_picture($request->file('car_back'));
        if(! empty($request->driver_licence_front)) $pro->driver_licence_front  = Helper::upload_picture($request->file('driver_licence_front'));
        if(! empty($request->driver_licence_back)) $pro->driver_licence_back    = Helper::upload_picture($request->file('driver_licence_back'));
        if(! empty($request->car_licence_front)) $pro->car_licence_front        = Helper::upload_picture($request->file('car_licence_front'));
        if(! empty($request->car_licence_back)) $pro->car_licence_back          = Helper::upload_picture($request->file('car_licence_back'));
        if(! empty($request->identity_front)) $pro->identity_front              = Helper::upload_picture($request->file('identity_front'));
        if(! empty($request->identity_back)) $pro->identity_back                = Helper::upload_picture($request->file('identity_back'));
        if(! empty($request->criminal_feat)) $pro->criminal_feat                = Helper::upload_picture($request->file('criminal_feat'));
        if(! empty($request->drug_analysis_licence)) $pro->drug_analysis_licence = Helper::upload_picture($request->file('drug_analysis_licence'));
        return $pro->save();
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Provider  $provider
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $provider = Provider::findOrFail($id);
            return view('admin.providers.provider-details', compact('provider'));
        } catch (ModelNotFoundException $e) {
            return $e;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Provider  $provider
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $provider = Provider::findOrFail($id);
            $service_types=DB::table('service_types')->get();
            $fleet=DB::table('fleets')->get();
            return view('admin.providers.edit',compact('provider','service_types','fleet'));
        } catch (ModelNotFoundException $e) {
            return $e;
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Provider  $provider
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(Setting::get('demo_mode', 0) == 1) {
            return back()->with('flash_error', 'Disabled for demo purposes! Please contact us at info@appoets.com');
        }

        $this->validate($request, [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'mobile' => 'between:11,13',
            'avatar' => 'mimes:jpeg,jpg,bmp,png|max:5242880',
            'fleet' => 'required',
            'service_type' => 'required',
            'service_number' => 'required|min:6|max:8',
            'service_model' => 'required',
        ]);

        try {

            $provider = Provider::findOrFail($id);

            if($request->hasFile('avatar')) {
                if($provider->avatar) {
                    Storage::delete($provider->avatar);
                }
                $provider->avatar = $request->avatar->store('uploads');
            }

            $provider->first_name = $request->first_name;
            $provider->last_name = $request->last_name;
            $provider->mobile = $request->mobile;
            $provider->fleet = $request->fleet;
            $provider->save();
            $provider->serviceTypes()->toggle($request->service_type);

            $this->saveProviderImages($provider, $request);

            return redirect()->route('admin.provider.index')->with('flash_success', 'Provider Updated Successfully');    
        } 

        catch (ModelNotFoundException $e) {
            return back()->with('flash_error', 'Provider Not Found');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Provider  $provider
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Setting::get('demo_mode', 0) == 1) {
            return back()->with('flash_error', 'Disabled for demo purposes! Please contact us at info@appoets.com');
        }

        try {
            Provider::find($id)->delete();
            return back()->with('message', 'Provider deleted successfully');
        } 
        catch (Exception $e) {
            return back()->with('flash_error', 'Provider Not Found');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Provider  $provider
     * @return \Illuminate\Http\Response
     */
    public function approve($id)
    {
        try {
            $Provider = Provider::findOrFail($id);
            if($Provider->update(['status' => 'approved'])){
                $Provider->service->update(['status' => 'active']);
                return back()->with('flash_success', "Provider Approved");
            } else {
                return redirect()->route('admin.provider.document.index', $id)->with('flash_error', "Provider has not been assigned a service type!");
            }
        } catch (ModelNotFoundException $e) {
            return back()->with('flash_error', "Something went wrong! Please try again later.");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Provider  $provider
     * @return \Illuminate\Http\Response
     */
    public function disapprove($id)
    {
        if(Setting::get('demo_mode', 0) == 1) {
            return back()->with('flash_error', 'Disabled for demo purposes! Please contact us at info@appoets.com');
        }
        $Provider = Provider::findOrFail($id);
        $Provider->update(['status' => 'banned']);{
        $Provider->service->update(['status' => 'offline']);
        return back()->with('flash_success', "Provider Disapproved");
    }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Provider  $provider
     * @return \Illuminate\Http\Response
     */
    public function request($id){

        try{

            $requests = UserRequests::where('user_requests.provider_id',$id)
                    ->RequestHistory()
                    ->get();

            return view('admin.request.index', compact('requests'));
        } catch (Exception $e) {
            return back()->with('flash_error','Something Went Wrong!');
        }
    }

    /**
     * account statements.
     *
     * @param  \App\Provider  $provider
     * @return \Illuminate\Http\Response
     */
    public function statement($id){

        try{

            $requests = UserRequests::where('provider_id',$id)
                        ->where('status','COMPLETED')
                        ->with('payment')
                        ->get();

            $rides = UserRequests::where('provider_id',$id)->with('payment')->orderBy('id','desc')->paginate(10);
            $cancel_rides = UserRequests::where('status','CANCELLED')->where('provider_id',$id)->count();
            $Provider = Provider::find($id);
            $revenue = UserRequestPayment::whereHas('request', function($query) use($id) {
                                    $query->where('provider_id', $id );
                                })->select(\DB::raw(
                                   'SUM(ROUND(fixed) + ROUND(distance)) as overall, SUM(ROUND(commision)) as commission' 
                               ))->get();


            $Joined = $Provider->created_at ? '- Joined '.$Provider->created_at->diffForHumans() : '';

            return view('admin.providers.statement', compact('rides','cancel_rides','revenue'))
                        ->with('page',$Provider->first_name."'s Overall Statement ". $Joined);

        } catch (Exception $e) {
            return back()->with('flash_error','Something Went Wrong!');
        }
    }

    public function Accountstatement($id){

        try{

            $requests = UserRequests::where('provider_id',$id)
                        ->where('status','COMPLETED')
                        ->with('payment')
                        ->get();

            $rides = UserRequests::where('provider_id',$id)->with('payment')->orderBy('id','desc')->paginate(10);
            $cancel_rides = UserRequests::where('status','CANCELLED')->where('provider_id',$id)->count();
            $Provider = Provider::find($id);
            $revenue = UserRequestPayment::whereHas('request', function($query) use($id) {
                                    $query->where('provider_id', $id );
                                })->select(\DB::raw(
                                   'SUM(ROUND(fixed) + ROUND(distance)) as overall, SUM(ROUND(commision)) as commission' 
                               ))->get();


            $Joined = $Provider->created_at ? '- Joined '.$Provider->created_at->diffForHumans() : '';

            return view('account.providers.statement', compact('rides','cancel_rides','revenue'))
                        ->with('page',$Provider->first_name."'s Overall Statement ". $Joined);

        } catch (Exception $e) {
            return back()->with('flash_error','Something Went Wrong!');
        }
    }
}
