<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

use App\Helpers\Helper;

use Auth;
use Setting;
use Exception;
use \Carbon\Carbon;
use App\ServiceConditions;
use App\User;
use App\Box;
use App\Fleet;
use App\Admin;
use App\Provider;
use App\UserPayment;
use App\ServiceType;
use App\UserRequests;
use App\ProviderService;
use App\UserRequestRating;
use App\UserRequestPayment;


class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * Dashboard.
     *
     * @param  \App\Provider  $provider
     * @return \Illuminate\Http\Response
     */
    public function dashboard()
    {
        try{

            $rides = UserRequests::has('user')->orderBy('id','desc')->get();
            $cancel_rides = UserRequests::where('status','CANCELLED');
            $scheduled_rides = UserRequests::where('status','SCHEDULED')->count();
            $provider_cancelled = UserRequests::where('status','CANCELLED')->where('cancelled_by','PROVIDER')->count();
            $user_cancelled = UserRequests::where('status','CANCELLED')->where('cancelled_by','USER')->count();
            $cancel_rides = $cancel_rides->count();
            $service = ServiceType::count();
            $fleet = Fleet::count();
            $revenue = UserRequestPayment::sum('total');
            $providers = Provider::take(10)->orderBy('rating','desc')->get();
            return view('admin.dashboard',compact('providers','fleet','scheduled_rides','service','rides','user_cancelled','provider_cancelled','cancel_rides','revenue'));
        }
        catch(Exception $e){
            return redirect()->route('admin.user.index')->with('flash_error','Something Went Wrong with Dashboard!');
        }
    }


    /**
     * Heat Map.
     *
     * @param  \App\Provider  $provider
     * @return \Illuminate\Http\Response
     */
    public function heatmap()
    {
        try{
            $rides = UserRequests::has('user')->orderBy('id','desc')->get();
            $providers = Provider::take(10)->orderBy('rating','desc')->get();
            return view('admin.heatmap',compact('providers','rides'));
        }
        catch(Exception $e){
            return redirect()->route('admin.user.index')->with('flash_error','Something Went Wrong with Dashboard!');
        }
    }

    /**
     * Map of all Users and Drivers.
     *
     * @return \Illuminate\Http\Response
     */
    public function map_index()
    {
        $users = '';
        $providers = Provider::whereHas('service',function($service){
            $service->where(['status' => 'active']);
        })->get();
        return view('admin.map.index',compact('providers'));
    }

    /**
     * Map of all Users and Drivers.
     *
     * @return \Illuminate\Http\Response
     */
    public function map_ajax()
    {
        try {

            $Providers = Provider::where('latitude', '!=', 0)
                    ->where('longitude', '!=', 0)
                    ->with('service')
                    ->get();

            $Users = User::where('latitude', '!=', 0)
                    ->where('longitude', '!=', 0)
                    ->get();

            for ($i=0; $i < sizeof($Users); $i++) { 
                $Users[$i]->status = 'user';
            }

            $All = $Users->merge($Providers);

            return $All;

        } catch (Exception $e) {
            return [];
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Provider  $provider
     * @return \Illuminate\Http\Response
     */
    public function settings()
    {
        return view('admin.settings.application');
    }

    public function dash_settings()
    {
        return view('admin.settings.dash_application');
    }

    public function condition_settings()
    {
        $conditions=ServiceConditions::all();
        return view('admin.condition.index',compact('conditions'));
    }
    public function condition_destroy(Request $request){

        try{
            ServiceConditions::findOrFail($_POST['id'])->delete();
            return back()->with('flash_success','condition deleted Successfully');
        } catch (ModelNotFoundException $e) {
            return back()->with('flash_error', 'Condition Not Found');
        }
    }


    public function condition_settings_store(Request $request)
    {
        $con=new ServiceConditions();
        $con->title= $request->title;
        $con->title_en= $request->title_en;
        $con->details= $request->details;
        $con->details_en= $request->details_en;
        $con->status= true;

        $con->save();

        return back()->with('flash_success','Condition Added Successfully');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Provider  $provider
     * @return \Illuminate\Http\Response
     */
    public function settings_store(Request $request)
    {
        if(Setting::get('demo_mode', 0) == 1) {
            return back()->with('flash_error','Disabled for demo purposes! Please contact us at info@appoets.com');
        }
        $this->validate($request,[
                'site_title' => 'required',
                'site_icon' => 'mimes:jpeg,jpg,bmp,png|max:5242880',
                'site_logo' => 'mimes:jpeg,jpg,bmp,png|max:5242880',
                'site_splash' => 'mimes:jpeg,jpg,bmp,png|max:5242880',
            ]);

        if($request->hasFile('site_icon')) {
            $site_icon = Helper::upload_picture($request->file('site_icon'));
            Setting::set('site_icon', $site_icon);
        }

        if($request->hasFile('site_logo')) {
            $site_logo = Helper::upload_picture($request->file('site_logo'));
            Setting::set('site_logo', $site_logo);
        }

        if($request->hasFile('site_splash')) {
            $site_splash = Helper::upload_picture($request->file('site_splash'));
            Setting::set('site_splash', $site_splash);
        }



        Setting::set('site_title', $request->site_title);
        Setting::set('site_title_en', $request->site_title_en);
        Setting::set('app_status', $request->app_status);
        Setting::set('app_msg', $request->app_msg);
        Setting::set('store_link_android', $request->store_link_android);
        Setting::set('store_link_ios', $request->store_link_ios);
        Setting::set('provider_select_timeout', $request->provider_select_timeout);
        Setting::set('provider_search_radius', $request->provider_search_radius);
        Setting::set('sos_number', $request->sos_number);
        Setting::set('contact_number', $request->contact_number);
        Setting::set('contact_email', $request->contact_email);
        Setting::set('site_copyright', $request->site_copyright);
        Setting::set('social_login', $request->social_login);
        Setting::set('interval_time', $request->interval_time);
        Setting::set('search_title', $request->search_title);
        Setting::set('search_title_en', $request->search_title_en);
        Setting::set('address', $request->address);




        //          Social Links
        Setting::set('facebook_link', $request->facebook_link);
        Setting::set('twitter_link', $request->twitter_link);
        Setting::set('youtube_link', $request->youtube_link);

        //          End Social Links
        Setting::save();
        
        return back()->with('flash_success','Settings Updated Successfully');
    }


    public function dash_settings_store(Request $request)
    {
        if(Setting::get('demo_mode', 0) == 1) {
            return back()->with('flash_error','Disabled for demo purposes! Please contact us at info@appoets.com');
        }



        if($request->hasFile('First_site_photo')) {
            $First_site_photo = Helper::upload_picture($request->file('First_site_photo'));
            Setting::set('First_site_photo', $First_site_photo);
        }

        Setting::set('text_first', $request->text_first);
        Setting::set('text_first_en', $request->text_first_en);

        if($request->hasFile('Second_site_photo')) {
            $Second_site_photo = Helper::upload_picture($request->file('Second_site_photo'));
            Setting::set('Second_site_photo', $Second_site_photo);
        }

        Setting::set('about_title', $request->about_title);
        Setting::set('about_title_en', $request->about_title_en);
        Setting::set('about_small_title', $request->about_small_title);
        Setting::set('about_small_title_en', $request->about_small_title_en);



        //first
        if($request->hasFile('First_about_photo')) {
            $First_about_photo = Helper::upload_picture($request->file('First_about_photo'));
            Setting::set('First_about_photo', $First_about_photo);
        }
        Setting::set('first_name', $request->first_name);
        Setting::set('first_details', $request->first_details);
        Setting::set('first_name_en', $request->first_name_en);
        Setting::set('first_details_en', $request->first_details_en);

        //second
        if($request->hasFile('Second_about_photo')) {
            $Second_about_photo = Helper::upload_picture($request->file('Second_about_photo'));
            Setting::set('Second_about_photo', $Second_about_photo);
        }
        Setting::set('second_name', $request->second_name);
        Setting::set('second_details', $request->second_details);
        Setting::set('second_name_en', $request->second_name_en);
        Setting::set('second_details_en', $request->second_details_en);


        //third
        if($request->hasFile('Third_about_photo')) {
            $Third_about_photo = Helper::upload_picture($request->file('Third_about_photo'));
            Setting::set('Third_about_photo', $Third_about_photo);
        }
        Setting::set('third_name', $request->third_name);
        Setting::set('third_details', $request->third_details);
        Setting::set('third_name_en', $request->third_name_en);
        Setting::set('third_details_en', $request->third_details_en);




        if($request->hasFile('footer_photo')) {
        $footer_photo = Helper::upload_picture($request->file('footer_photo'));
        Setting::set('footer_photo', $footer_photo);
    }

//       login user backgruond photo and title
        if($request->hasFile('user_backgruond_photo')) {
            $user_backgruond_photo = Helper::upload_picture($request->file('user_backgruond_photo'));
            Setting::set('user_backgruond_photo', $user_backgruond_photo);
        }

        Setting::set('big_title_ar', $request->big_title_ar);
        Setting::set('big_title_en', $request->big_title_en);
        Setting::set('small_title_ar', $request->small_title_ar);
        Setting::set('small_title_en', $request->small_title_en);



        //       login provider backgruond photo and title
        if($request->hasFile('provider_backgruond_photo')) {
            $provider_backgruond_photo = Helper::upload_picture($request->file('provider_backgruond_photo'));
            Setting::set('provider_backgruond_photo', $provider_backgruond_photo);
        }

        Setting::set('provider_big_title_ar', $request->provider_big_title_ar);
        Setting::set('provider_big_title_en', $request->provider_big_title_en);
        Setting::set('provider_small_title_ar', $request->provider_small_title_ar);
        Setting::set('provider_small_title_en', $request->provider_small_title_en);
        Setting::set('provider_ditals_ar', $request->provider_ditals_ar);
        Setting::set('provider_ditals_en', $request->provider_ditals_en);


        Setting::save();

        return back()->with('flash_success','Settings Updated Successfully');
    }

    public function box_settings_store(Request $request)
    {
        $box=new Box();
        if($request->hasFile('box_photo')) {
            $box_photo = Helper::upload_picture($request->file('box_photo'));
            $box->photo=$box_photo;
        }
        $box->title= $request->box_title;
        $box->title_en= $request->box_title_en;
        $box->details= $request->box_details;
        $box->details_en= $request->box_details_en;
        $box->link= $request->box_link;

        $box->save();

        return back()->with('flash_success','Box Added Successfully');
    }

    public function box_delete(Request $request)
    {
        Box::findOrFail($_POST['box_id'])->delete();

        return back()->with('flash_success','Box deleted Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Provider  $provider
     * @return \Illuminate\Http\Response
     */
    public function settings_payment()
    {
        return view('admin.payment.settings');
    }

    /**
     * Save payment related settings.
     *
     * @param  \App\Provider  $provider
     * @return \Illuminate\Http\Response
     */
    public function settings_payment_store(Request $request)
    {
        // dd($request->all());
        if(Setting::get('demo_mode', 0) == 1) {
            return back()->with('flash_error', 'Disabled for demo purposes! Please contact us at info@appoets.com');
        }

        $this->validate($request, [
                'CARD' => 'in:on',
                'CASH' => 'in:on',
                'stripe_secret_key' => 'required_if:CARD,on|max:255',
                'stripe_publishable_key' => 'required_if:CARD,on|max:255',
                'daily_target' => 'required|integer|min:0',
                'tax_percentage' => 'required|numeric|min:0|max:100',
                'surge_percentage' => 'required|numeric|min:0|max:100',
                'commission_percentage' => 'required|numeric|min:0|max:100',
                'surge_trigger' => 'required|integer|min:0',
                'currency' => 'required'
            ]);

        Setting::set('CARD', $request->has('CARD') ? 1 : 0 );
        Setting::set('CASH', $request->has('CASH') ? 1 : 0 );
        Setting::set('stripe_secret_key', $request->stripe_secret_key);
        Setting::set('stripe_publishable_key', $request->stripe_publishable_key);
        Setting::set('daily_target', $request->daily_target);
        Setting::set('tax_percentage', $request->tax_percentage);
        Setting::set('surge_percentage', $request->surge_percentage);
        Setting::set('commission_percentage', $request->commission_percentage);
        Setting::set('surge_trigger', $request->surge_trigger);
        Setting::set('currency', $request->currency);
        Setting::set('booking_prefix', $request->booking_prefix);
        Setting::save();

        return back()->with('flash_success','Settings Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Provider  $provider
     * @return \Illuminate\Http\Response
     */
    public function profile()
    {
        return view('admin.account.profile');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Provider  $provider
     * @return \Illuminate\Http\Response
     */
    public function profile_update(Request $request)
    {
        if(Setting::get('demo_mode', 0) == 1) {
            return back()->with('flash_error', 'Disabled for demo purposes! Please contact us at info@appoets.com');
        }

        $this->validate($request,[
            'name' => 'required|max:255',
            'email' => 'email',
            'picture' => 'mimes:jpeg,jpg,bmp,png',
        ]);

        try{
            $admin = Auth::guard('admin')->user();
            $admin->name = $request->name;
            $admin->email = $request->email;
            if($request->hasFile('picture')){
                $admin->picture = $request->picture->store('uploads');
            }

            $admin->save();

            return redirect()->back()->with('flash_success','Successfully Profile Updated ');
        }

        catch (Exception $e) {
             return back()->with('flash_error','Something Went Wrong!');
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Provider  $provider
     * @return \Illuminate\Http\Response
     */
    public function password()
    {
        return view('admin.account.change-password');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Provider  $provider
     * @return \Illuminate\Http\Response
     */
    public function password_update(Request $request)
    {
        if(Setting::get('demo_mode', 0) == 1) {
            return back()->with('flash_error','Disabled for demo purposes! Please contact us at info@appoets.com');
        }

        $this->validate($request,[
            'old_password' => 'required',
            'password' => 'required|min:6|confirmed',
        ]);

        try {

           $Admin = Admin::find(Auth::guard('admin')->user()->id);

            if(password_verify($request->old_password, $Admin->password))
            {
                $Admin->password = bcrypt($request->password);
                $Admin->save();

                return redirect()->back()->with('flash_success','Successfully Password Updated');
            }
        } catch (Exception $e) {
             return back()->with('flash_error','Something Went Wrong!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Provider  $provider
     * @return \Illuminate\Http\Response
     */
    public function payment()
    {
        try {
             $payments = UserRequests::where('paid', 1)
                    ->has('user')
                    ->has('provider')
                    ->has('payment')
                    ->orderBy('user_requests.created_at','desc')
                    ->get();
            
            return view('admin.payment.payment-history', compact('payments'));
        } catch (Exception $e) {
             return back()->with('flash_error','Something Went Wrong!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Provider  $provider
     * @return \Illuminate\Http\Response
     */
    public function help()
    {
        try {
            $str = file_get_contents('http://appoets.com/help.json');
            $Data = json_decode($str, true);
            return view('admin.help', compact('Data'));
        } catch (Exception $e) {
             return back()->with('flash_error','Something Went Wrong!');
        }
    }

    /**
     * User Rating.
     *
     * @return \Illuminate\Http\Response
     */
    public function user_review()
    {
        try {
            $Reviews = UserRequestRating::where('user_id', '!=', 0)->has('user', 'provider')->get();
            return view('admin.review.user_review',compact('Reviews'));
        } catch(Exception $e) {
            return redirect()->route('admin.setting')->with('flash_error','Something Went Wrong!');
        }
    }

    /**
     * Provider Rating.
     *
     * @return \Illuminate\Http\Response
     */
    public function provider_review()
    {
        try {
            $Reviews = UserRequestRating::where('provider_id','!=',0)->with('user','provider')->get();
            return view('admin.review.provider_review',compact('Reviews'));
        } catch(Exception $e) {
            return redirect()->route('admin.setting')->with('flash_error','Something Went Wrong!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProviderService
     * @return \Illuminate\Http\Response
     */
    public function destory_provider_service($id){
        try {
            ProviderService::find($id)->delete();
            return back()->with('message', 'Service deleted successfully');
        } catch (Exception $e) {
             return back()->with('flash_error','Something Went Wrong!');
        }
    }

    /**
     * Testing page for push notifications.
     *
     * @return \Illuminate\Http\Response
     */
    public function push_index()
    {
        $data = PushNotification::app('IOSUser')
            ->to('163e4c0ca9fe084aabeb89372cf3f664790ffc660c8b97260004478aec61212c')
            ->send('Hello World, i`m a push message');
        dd($data);

        $data = PushNotification::app('IOSProvider')
            ->to('a9b9a16c5984afc0ea5b681cc51ada13fc5ce9a8c895d14751de1a2dba7994e7')
            ->send('Hello World, i`m a push message');
        dd($data);
    }

    /**
     * Testing page for push notifications.
     *
     * @return \Illuminate\Http\Response
     */
    public function push_store(Request $request,$id)
    {
        try {
            ProviderService::find($id)->delete();
            return back()->with('message', 'Service deleted successfully');
        } catch (Exception $e) {
             return back()->with('flash_error','Something Went Wrong!');
        }
    }

    /**
     * privacy.
     *
     * @param  \App\Provider  $provider
     * @return \Illuminate\Http\Response
     */

    public function privacy(){
        return view('admin.pages.static')
            ->with('title',"Privacy Page")
            ->with('page', "privacy");
    }

    /**
     * pages.
     *
     * @param  \App\Provider  $provider
     * @return \Illuminate\Http\Response
     */
//    public function pages(Request $request){
//        $this->validate($request, [
//                'page' => 'required|in:page_privacy',
//                'content' => 'required',
//            ]);
//
//        Setting::set($request->page, $request->content);
//        Setting::save();
//
//        return back()->with('flash_success', 'Content Updated!');
//    }

    /**
     * account statements.
     *
     * @param  \App\Provider  $provider
     * @return \Illuminate\Http\Response
     */
    public function statement($type = 'individual'){

        try{

            $page = 'Ride Statement';

            if($type == 'individual'){
                $page = 'Provider Ride Statement';
            }elseif($type == 'today'){
                $page = 'Today Statement - '. date('d M Y');
            }elseif($type == 'monthly'){
                $page = 'This Month Statement - '. date('F');
            }elseif($type == 'yearly'){
                $page = 'This Year Statement - '. date('Y');
            }

            $rides = UserRequests::with('payment')->orderBy('id','desc');
            $cancel_rides = UserRequests::where('status','CANCELLED');
            $revenue = UserRequestPayment::select(\DB::raw(
                           'SUM(ROUND(fixed) + ROUND(distance)) as overall, SUM(ROUND(commision)) as commission' 
                       ));

            if($type == 'today'){

                $rides->where('created_at', '>=', Carbon::today());
                $cancel_rides->where('created_at', '>=', Carbon::today());
                $revenue->where('created_at', '>=', Carbon::today());

            }elseif($type == 'monthly'){

                $rides->where('created_at', '>=', Carbon::now()->month);
                $cancel_rides->where('created_at', '>=', Carbon::now()->month);
                $revenue->where('created_at', '>=', Carbon::now()->month);

            }elseif($type == 'yearly'){

                $rides->where('created_at', '>=', Carbon::now()->year);
                $cancel_rides->where('created_at', '>=', Carbon::now()->year);
                $revenue->where('created_at', '>=', Carbon::now()->year);

            }

            $rides = $rides->get();
            $cancel_rides = $cancel_rides->count();
            $revenue = $revenue->get();

            return view('admin.providers.statement', compact('rides','cancel_rides','revenue'))
                    ->with('page',$page);

        } catch (Exception $e) {
            return back()->with('flash_error','Something Went Wrong!');
        }
    }


    /**
     * account statements today.
     *
     * @param  \App\Provider  $provider
     * @return \Illuminate\Http\Response
     */
    public function statement_today(){
        return $this->statement('today');
    }

    /**
     * account statements monthly.
     *
     * @param  \App\Provider  $provider
     * @return \Illuminate\Http\Response
     */
    public function statement_monthly(){
        return $this->statement('monthly');
    }

     /**
     * account statements monthly.
     *
     * @param  \App\Provider  $provider
     * @return \Illuminate\Http\Response
     */
    public function statement_yearly(){
        return $this->statement('yearly');
    }


    /**
     * account statements.
     *
     * @param  \App\Provider  $provider
     * @return \Illuminate\Http\Response
     */
    public function statement_provider(){

        try{

            $Providers = Provider::all();

            foreach($Providers as $index => $Provider){

                $Rides = UserRequests::where('provider_id',$Provider->id)
                            ->where('status','<>','CANCELLED')
                            ->get()->pluck('id');

                $Providers[$index]->rides_count = $Rides->count();

                $Providers[$index]->payment = UserRequestPayment::whereIn('request_id', $Rides)
                                ->select(\DB::raw(
                                   'SUM(ROUND(fixed) + ROUND(distance)) as overall, SUM(ROUND(commision)) as commission' 
                                ))->get();
            }

            return view('admin.providers.provider-statement', compact('Providers'))->with('page','Providers Statement');

        } catch (Exception $e) {
            return back()->with('flash_error','Something Went Wrong!');
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Provider  $provider
     * @return \Illuminate\Http\Response
     */
    public function translation()
    {
        try{
            return view('admin.translation');
        }
        catch (Exception $e) {
             return back()->with('flash_error','Something Went Wrong!');
        }
    }
    

    public function condition_ChangeStatus(Request $request){
        $con = ServiceConditions::all();
        $conn = ServiceConditions::findOrFail($_POST['con_id']);
        if($request->ajax()) {
            $conn->status=$_POST['status'];
            $conn->save();
            return $conn;
        } else {
            return view('admin.condition.index', compact('con'));
        }
    }

    public function Change_Status(Request $request){
        $Model = '\App\\'.$request->model;
        $Model::where('id',$request->id)->update(['status' => ($Model::where('id',$request->id)->first()->status -1)*-1]);
    }
}
