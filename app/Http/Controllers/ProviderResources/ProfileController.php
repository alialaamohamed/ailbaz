<?php

namespace App\Http\Controllers\ProviderResources;

use App\ProviderDevice;
use App\ServiceType;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;

use App\Http\Controllers\Controller;

use DB;
use Auth;
use Setting;
use Storage;
use Exception;
use Carbon\Carbon;

use App\ProviderProfile;
use App\UserRequests;
use App\Provider;
use App\ProviderService;
use App\Fleet;

class ProfileController extends Controller
{
    /**
     * Create a new user instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('provider.api', ['except' => ['show', 'store', 'available', 'location_edit', 'location_update', 'checkAccount']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        try {

            Auth::user()->service = ProviderService::where('provider_id', Auth::user()->id)
                ->with('service_type')
                ->first();
            Auth::user()->fleet = Fleet::find(Auth::user()->fleet);
            Auth::user()->currency = Setting::get('currency', '$');
            Auth::user()->sos = Setting::get('sos_number', '911');

            return Auth::user();

        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function newDetails(Request $request)
    {
        $validator = validator()->make($request->all(), [
            'token' => 'required',
        ]);

        if ($validator->fails()) {
            return responseJson(0, $validator->errors()->first(), $validator->errors());
        }
        $provider = ProviderDevice::with('provider')->with('service_type')->where('token' , $request->token)->first();
        if ($provider) {
            $provider->provider;
            $provider->provider->fleet = Fleet::find(Auth::user()->fleet);
            $provider->provider->currency = Setting::get('currency', '$');
            $provider->provider->sos = Setting::get('sos_number', '911');


            return responseJson(1, 'تمت العمليه بنجاح', $provider);
        }
        return responseJson(0, 'لا توجد بيانات');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */

    public function checkAccount(Request $request)
    {
        $this->validate($request, [
            'mail' => 'required|email',
        ]);
        try {
            $Provider = Provider::where(['email' => $request->mail])->get();
            if (count($Provider) > 0) {
                return response()->json(true);
            } else {
                return response()->json(false);
            }
        } catch (ModelNotFoundException $e) {
            return response()->json(['error' => 'Account Error'], 404);
        }
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required|min:3|max:255',
            'last_name' => 'required|min:3|max:255',
            'mobile' => 'required||between:13,13',
            'avatar' => 'mimes:jpeg,jpg,bmp,png',
            'language' => 'required|max:255',
            'address' => 'required|min:3|max:255',
            'address_secondary' => 'required|min:10|max:255',
            'city' => 'required|min:5|max:255',
            'country' => 'required|min:3|max:255',
            'postal_code' => 'required|min:16|max:255',
            'car_number' => 'min:6|max:8',
            'car_history' => 'min:4|max:4',
        ]);

        try {

            $Provider = Auth::user();

            if ($request->has('first_name'))
                $Provider->first_name = $request->first_name;

            if ($request->has('last_name'))
                $Provider->last_name = $request->last_name;

            if ($request->has('mobile'))
                $Provider->mobile = $request->mobile;

            if ($request->hasFile('avatar')) {
                Storage::delete($Provider->avatar);
                $Provider->avatar = $request->avatar->store('provider/profile');

            }

            if ($request->has('car_type_id')) {
                if ($Provider->service) {
                    if ($Provider->service->car_type_id != $request->car_type) {
                        $Provider->status = 'banned';
                    }
                    $ProviderService = ProviderService::find(Auth::user()->id);
                    $ProviderService->car_type_id = $request->car_type;
                    $ProviderService->car_number = $request->car_number;
                    $ProviderService->car_history = $request->car_history;
                    $ProviderService->save();
                } else {
                    ProviderService::create([
                        'provider_id' => $Provider->id,
                        'car_type_id' => $request->car_type,
                        'car_number' => $request->car_number,
                        'car_history' => $request->car_history,
                    ]);
                    $Provider->status = 'banned';
                }
            }

            if ($Provider->profile) {
                $Provider->profile->update([
                    'language' => $request->language ?: $Provider->profile->language,
                    'address' => $request->address ?: $Provider->profile->address,
                    'address_secondary' => $request->address_secondary ?: $Provider->profile->address_secondary,
                    'city' => $request->city ?: $Provider->profile->city,
                    'country' => $request->country ?: $Provider->profile->country,
                    'postal_code' => $request->postal_code ?: $Provider->profile->postal_code,
                    'car_number' => $request->car_number ?: $Provider->profile->car_number,
                    'car_history' => $request->car_history ?: $Provider->profile->car_history,
                ]);
            } else {
                ProviderProfile::create([
                    'provider_id' => $Provider->id,
                    'language' => $request->language,
                    'address' => $request->address,
                    'address_secondary' => $request->address_secondary,
                    'city' => $request->city,
                    'country' => $request->country,
                    'postal_code' => $request->postal_code,
                    'car_number' => $request->car_number,
                    'car_history' => $request->car_history,
                ]);
            }

            $Provider->save();

            return redirect(route('provider.profile.index'))->with('flash_success', 'Provider Updated Successfully');
        } catch (ModelNotFoundException $e) {
            return response()->json(['error' => 'Provider Not Found!'], 404);
        }
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $car_models=DB::table('car_models')->get();
        return view('provider.profile.index',compact('car_models'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */


    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'mobile' => 'required|between:13,13',
            'avatar' => 'required|mimes:jpeg,png,png',
            'language' => 'required|max:255',
            'address' => 'required|max:255',
            'address_secondary' => 'required|max:255',
            'city' => 'required|max:255',
            'country' => 'required|max:255',
            'postal_code' => 'required|max:255',
            'service_number' => 'required|max:255',
            'service_model' => 'required|max:255',
        ]);
        try {

            $Provider = Auth::user();

            if ($request->has('first_name'))
                $Provider->first_name = $request->first_name;

            if ($request->has('last_name'))
                $Provider->last_name = $request->last_name;

            if ($request->has('mobile'))
                $Provider->mobile = $request->mobile;

            if ($request->hasFile('avatar')) {
                Storage::delete($Provider->avatar);
                $Provider->avatar = $request->avatar->store('uploads');

            }

            if ($Provider->profile) {
                $Provider->profile->update([
                    'language' => $request->language ?: $Provider->profile->language,
                    'address' => $request->address ?: $Provider->profile->address,
                    'address_secondary' => $request->address_secondary ?: $Provider->profile->address_secondary,
                    'city' => $request->city ?: $Provider->profile->city,
                    'country' => $request->country ?: $Provider->profile->country,
                    'postal_code' => $request->postal_code ?: $Provider->profile->postal_code,
                ]);
            } else {
                ProviderProfile::create([
                    'provider_id' => $Provider->id,
                    'language' => $request->language,
                    'address' => $request->address,
                    'address_secondary' => $request->address_secondary,
                    'city' => $request->city,
                    'country' => $request->country,
                    'postal_code' => $request->postal_code,
                ]);
            }


            $Provider->save();

            return $Provider;
        } catch (ModelNotFoundException $e) {
            return response()->json(['error' => 'Provider Not Found!'], 404);
        }
    }

    /**
     * Update latitude and longitude of the user.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function location(Request $request)
    {
        $this->validate($request, [
            'latitude' => 'required|numeric',
            'longitude' => 'required|numeric',
        ]);

        if ($Provider = Auth::user()) {

            $Provider->latitude = $request->latitude;
            $Provider->longitude = $request->longitude;
            $Provider->save();

            return response()->json(['message' => 'Location Updated successfully!']);

        } else {
            return response()->json(['error' => 'Provider Not Found!']);
        }
    }

    /**
     * Toggle service availability of the provider.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function available(Request $request)
    {

        $this->validate($request, [
            'service_status' => 'required|in:active,offline',
            'latitude' => 'required|numeric',
            'longitude' => 'required|numeric',
        ]);

        $Provider = Auth::user();
//        dd($Provider->longitude);

        if ($Provider->status == 'approved' && $request->service_status == 'offline') {
            $Provider->service->update(['status' => $request->service_status]);
        } elseif ($Provider->status == 'approved' && $request->service_status == 'active') {
            $Provider->service->update(['status' => $request->service_status]);
            $Provider->update(['latitude' => $request->latitude, 'longitude' => $request->longitude]);
        } else {
            return response()->json(['error' => 'You account has not been approved for driving']);
        }
        return $Provider->service;
    }

    /**
     * Update password of the provider.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function password(Request $request)
    {
        $this->validate($request, [
            'password' => 'required|confirmed',
            'password_old' => 'required',
        ]);

        $Provider = Auth::user();

        if (password_verify($request->password_old, $Provider->password)) {
            $Provider->password = bcrypt($request->password);
            $Provider->save();

            return response()->json(['message' => 'Password changed successfully!']);
        } else {
            return response()->json(['error' => 'Please enter correct password'], 422);
        }
    }

    /**
     * Show providers daily target.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function target(Request $request)
    {
        try {

            $Rides = UserRequests::where('provider_id', Auth::user()->id)
                ->where('status', 'COMPLETED')
                ->where('created_at', '>=', Carbon::today())
                ->with('payment', 'service_type')
                ->get();

            return response()->json([
                'rides' => $Rides,
                'rides_count' => $Rides->count(),
                'target' => Setting::get('daily_target', '0')
            ]);

        } catch (Exception $e) {
            return response()->json(['error' => "Something Went Wrong"]);
        }
    }

    public function App_Details()
    {

        $App_Name = Setting::get('site_title', 'Al Baz');
//        dd($App_Name);
        $App_Icon = Setting::get('site_icon', 'http://ailbaz.com/public/logo-black.png');
        $App_Splash = Setting::get('splash', 'http://ailbaz.com/public/logo-black.png');
        $App_Logo = Setting::get('site_logo', 'http://ailbaz.com/public/logo-black.png');
        $App_Status = Setting::get('app_status', '1');
        $App_Offline_Msg = Setting::get('offline_msg', 'System is under Maintenance');
        $Phone_Number = Setting::get('contact_number', '');
        $Email = Setting::get('contact_email', '');
        $Interval_Time = Setting::get('interval_time', '3000');
        $time_left_to_respond = Setting::get('provider_select_timeout', '180');
        $provider_search_radius = Setting::get('provider_search_radius', '2000');
        $app_msg = Setting::get('app_msg', '');

        return response()->json([
            'App_Name' => $App_Name,
            'App_Icon' => $App_Icon,
            'App_Logo' => $App_Logo,
            'App_Splash' => $App_Splash,
            'App_Status' => $App_Status,
            'App_Offline_Msg' => $App_Offline_Msg,
            'App_Msg' => $app_msg,
            'Phone_Number' => $Phone_Number,
            'Email' => $Email,
            'Interval_Time' => $Interval_Time,
            'Time_Left_To_Respond' => $time_left_to_respond,
            'Searching_Range' => $provider_search_radius,
        ]);
    }

    public function updateLocation(Request $request)
    {
        $validator = validator()->make($request->all(), [
            'token' => 'required',
            'latitude' => 'required|numeric',
            'longitude' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return responseJson(0, $validator->errors()->first(), $validator->errors());
        }
        $provider = ProviderDevice::with('provider')->where('token' , $request->token)->first();
        if ($provider) {
            $provider->provider->update(['latitude' => $request->latitude, 'longitude' => $request->longitude]);


            return responseJson(1, 'تمت العمليه بنجاح', $provider);
        }
        return responseJson(0, 'لا توجد بيانات');
    }
}
