<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransportationType extends Model
{
    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
        'name',
        'status',
        'image',
        'capacity',
    ];

    /**
    * The attributes that should be hidden for arrays.
    *
    * @var array
    */
    protected $hidden = [
         'created_at', 'updated_at'
    ];
}
