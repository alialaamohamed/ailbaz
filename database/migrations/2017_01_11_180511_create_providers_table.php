<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProvidersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        Schema::create('providers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email')->unique();
            $table->string('mobile')->nullable();
            $table->string('password');
            $table->string('avatar')->nullable();
            $table->string('car_front')->nullable();
            $table->string('logo')->nullable();
            $table->string('car_back')->nullable();
            $table->string('driver_licence_front')->nullable();
            $table->string('driver_licence_back')->nullable();
            $table->string('identity_front')->nullable();
            $table->string('identity_back')->nullable();
            $table->string('car_licence_front')->nullable();
            $table->string('car_licence_back')->nullable();
            $table->string('criminal_feat')->nullable();
            $table->decimal('rating', 4, 2)->default(5);
            $table->enum('status', ['onboarding', 'approved', 'banned']);
            $table->integer('fleet')->default(0);
            $table->double('latitude', 15, 8)->nullable();
            $table->double('longitude', 15, 8)->nullable();
            $table->mediumInteger('otp')->default(0);
            $table->string('id_licence')->nullable();
            $table->string('driver_licence')->nullable();
            $table->string('criminal_status_licence')->nullable();
            $table->string('drug_analysis_licence')->nullable();
            $table->timestamp('end_driver_licence')->nullable();
            $table->Integer('car_id')->nullable();
            $table->enum('login_by',array('manual','facebook','google'));
            $table->string('social_unique_id')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('providers');
    }
}
