@extends('provider.layout.auth')

@section('content')
<div class="col-md-12">
    <a class="log-blk-btn" href="{{ url('/provider/register') }}">@lang('user.create_new_account')</a>
    <h3>@lang('user.sing_in')</h3>
</div>

<div class="col-md-12">
    <form role="form" method="POST" action="{{ url('/provider/login') }}">
        {{ csrf_field() }}

        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="@lang('user.email_address')" autofocus>

        @if ($errors->has('email'))
            <span class="help-block">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif

        <input id="password" type="password" class="form-control" name="password" placeholder="@lang('user.pass')">

        @if ($errors->has('password'))
            <span class="help-block">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
        @endif

        <div class="checkbox">
            <label>
                <input type="checkbox" name="remember">@lang('user.remember_me')
            </label>
        </div>

        <br>

        <button type="submit" class="log-teal-btn">
        @lang('user.login')
        </button>

        <p class="helper"><a href="{{ url('/provider/password/reset') }}">@lang('user.forget_pass')</a></p>
        <p class="helper"> <a href="{{ url('/') }}">@lang('user.back')</a></p>
    </form>
    @if(Setting::get('social_login', 0) == 1)
    @endif
</div>
@endsection
