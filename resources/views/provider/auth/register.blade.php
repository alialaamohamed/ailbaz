@extends('provider.layout.auth')

@section('content')
<div class="col-md-12">
    <a class="log-blk-btn" href="{{ url('/provider/login') }}">@lang('user.already_registered')</a>
    <h3>@lang('user.sing_up')</h3>
</div>

<div class="col-md-12">
    <form class="form-horizontal" role="form" method="POST" action="{{ url('/provider/register') }}" enctype="multipart/form-data" >

        <div id="first_step">
            <div class="col-md-4">
                <input value="+2" type="text" placeholder="+2" id="country_code" name="country_code" />
            </div>

            <div class="col-md-8">
                <input type="text" autofocus id="phone_number" class="form-control" placeholder="01010209147" name="phone_number" value="{{ old('phone_number') }}" />
            </div>

            <div class="col-md-8">
                @if ($errors->has('phone_number'))
                    <span class="help-block">
                        <strong>{{ $errors->first('phone_number') }}</strong>
                    </span>
                @endif
            </div>

            <div class="col-md-12" style="padding-bottom: 10px;" id="mobile_verfication">
                <input type="button" class="log-teal-btn small" onclick="smsLogin();" value="@lang('user.verify_phone_number')"/>
            </div>
        </div>

        {{ csrf_field() }}

        <div id="second_step">

            <input id="name" type="text" class="form-control" name="first_name" value="{{ old('first_name') }}" placeholder="First Name" autofocus>

            @if ($errors->has('first_name'))
                <span class="help-block">
                    <strong>{{ $errors->first('first_name') }}</strong>
                </span>
            @endif

            <input id="name" type="text" class="form-control" name="last_name" value="{{ old('last_name') }}" placeholder="Last Name">

            @if ($errors->has('last_name'))
                <span class="help-block">
                    <strong>{{ $errors->first('last_name') }}</strong>
                </span>
            @endif

            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="E-Mail Address">

            @if ($errors->has('email'))
                <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif

            <input id="password" type="password" class="form-control" name="password" placeholder="Password">

            @if ($errors->has('password'))
                <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
            @endif

            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Confirm Password">

            @if ($errors->has('password_confirmation'))
                <span class="help-block">
                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                </span>
            @endif

            <select class="form-control" name="car_type" id="car_type">
                <option value="0">اختار نوع السيارة</option>
                @foreach( $car_models as $type)
                    <option value="{{$type->id}}">{{$type->name}}</option>
                @endforeach
            </select>
            @if ($errors->has('car_type'))
                <span class="help-block">
                    <strong>{{ $errors->first('car_type')}}</strong>
                </span>
            @endif
            <input id="car_history" type="text" class="form-control" name="car_history" value="{{ old('car_history')}}" placeholder="ادخل سنة الصنع">

            @if ($errors->has('car_history'))
                <span class="help-block">
                    <strong>{{ $errors->first('car_history') }}</strong>
                </span>
            @endif

            <input id="car_number" type="text" class="form-control" name="car_number" value="{{ old('car_number') }}" placeholder=" رقم السيارة">

            @if ($errors->has('car_number'))
                <span class="help-block">
                    <strong>{{ $errors->first('car_number') }}</strong>
                </span>
            @endif



            <div class="form-group col-md-12">
                <label> Logo </label>
                <input id="car-front-model" type="file" class="form-control" name="logo" value="" required placeholder="Logo">
                @if ($errors->has('logo'))
                    <span class="help-block">
                        <strong>{{ $errors->first('logo') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group col-md-12">
                <label> Card Front </label>
                <input id="car-front-model" type="file" class="form-control" name="car_front" value="" placeholder="Car Front">
                @if ($errors->has('car_front'))
                    <span class="help-block">
                        <strong>{{ $errors->first('car_front') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group col-md-12">
                <label> Card Back </label>
                <input id="car-front-model" type="file" class="form-control" name="car_back" value="" placeholder="Car Front">
                @if ($errors->has('car_back'))
                    <span class="help-block">
                        <strong>{{ $errors->first('car_back') }}</strong>
                    </span>
                @endif
            </div>


            <div class="form-group col-md-12">
                <label> Driver Licence Front </label>
                <input id="car-front-model" type="file" class="form-control" name="driver_licence_front" value="" placeholder="Driver Licence Front">
                @if ($errors->has('driver_licence_front'))
                    <span class="help-block">
                        <strong>{{ $errors->first('driver_licence_front') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group col-md-12">
                <label> Driver Licence Back </label>
                <input id="car-front-model" type="file" class="form-control" name="driver_licence_back" value="" placeholder="Driver Licence Back">
                @if ($errors->has('driver_licence_back'))
                    <span class="help-block">
                        <strong>{{ $errors->first('driver_licence_back') }}</strong>
                    </span>
                @endif
            </div>


            <div class="form-group col-md-12">
                <label> Car Licence Front </label>
                <input id="car-front-model" type="file" class="form-control" name="car_licence_front" value="" placeholder="Car Licence Front">
                @if ($errors->has('car_licence_front'))
                    <span class="help-block">
                        <strong>{{ $errors->first('car_licence_front') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group col-md-12">
                <label> Car Licence Back </label>
                <input id="car-front-model" type="file" class="form-control" name="car_licence_back" value="" placeholder="Car Licence Back">
                @if ($errors->has('car_licence_back'))
                    <span class="help-block">
                        <strong>{{ $errors->first('car_licence_back') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group col-md-12">
                <label> Identity Front </label>
                <input id="car-front-model" type="file" class="form-control" name="identity_front" value="" placeholder="Identity Front">
                @if ($errors->has('identity_front'))
                    <span class="help-block">
                        <strong>{{ $errors->first('identity_front') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group col-md-12">
                <label> Identity Back </label>
                <input id="car-front-model" type="file" class="form-control" name="identity_back" value="" placeholder="Identity Back">
                @if ($errors->has('identity_back'))
                    <span class="help-block">
                        <strong>{{ $errors->first('identity_back') }}</strong>
                    </span>
                @endif
            </div>


            <div class="form-group col-md-12">
                <label> criminal_feat </label>
                <input id="car-front-model" type="file" class="form-control" name="criminal_feat" value="" placeholder="criminal_feat">
                @if ($errors->has('criminal_feat'))
                    <span class="help-block">
                        <strong>{{ $errors->first('criminal_feat') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group col-md-12">
                <label> drug_analysis_licence </label>
                <input id="car-front-model" type="file" class="form-control" name="drug_analysis_licence" value="" placeholder="drug_analysis_licence">
                @if ($errors->has('drug_analysis_licence'))
                    <span class="help-block">
                        <strong>{{ $errors->first('drug_analysis_licence') }}</strong>
                    </span>
                @endif
            </div>


            <button type="submit" class="log-teal-btn">
                Register
            </button>

        </div>
    </form>
</div>
@endsection


@section('scripts')
<script src="https://sdk.accountkit.com/en_US/sdk.js"></script>
<script>
  // initialize Account Kit with CSRF protection
  AccountKit_OnInteractive = function(){
    AccountKit.init(
      {
        appId:{{env('FB_APP_ID')}},
        state:"state",
        version: "{{env('FB_APP_VERSION')}}",
        fbAppEventsEnabled:true,
        redirect:"http://192.168.1.200/ailbaz_server/public/provider/register",
        debug:true
      }
    );
  };

</script>
<script>

  // login callback
  function loginCallback(response) {
    if (response.status === "PARTIALLY_AUTHENTICATED") {
      var code = response.code;
      var csrf = response.state;
      // Send code to server to exchange for access token
      $('#mobile_verfication').html("<p class='helper'> * Phone Number Verified </p>");
      $('#phone_number').attr('readonly',true);
      $('#country_code').attr('readonly',true);
      $('#second_step').fadeIn(400);

      $.post("{{route('account.kit')}}",{ code : code }, function(data){
        $('#phone_number').val(data.phone.national_number);
        $('#country_code').val('+'+data.phone.country_prefix);
      });

    }
    else if (response.status === "NOT_AUTHENTICATED") {
      // handle authentication failure
      $('#mobile_verfication').html("<p class='helper'> * @lang('user.authentication_failed') </p>");
    }
    else if (response.status === "BAD_PARAMS") {
      // handle bad parameters
    }
  }
</script>

    <script>
  // phone form submission handler
  function smsLogin() {
    var countryCode = document.getElementById("country_code").value;
    var phoneNumber = document.getElementById("phone_number").value;

    $('#mobile_verfication').html("<p class='helper'> @lang('user.plz_wait') </p>");
    $('#phone_number').attr('readonly',true);
    $('#country_code').attr('readonly',true);

    AccountKit.login(
      'PHONE',
      {countryCode: countryCode, phoneNumber: phoneNumber}, // will use default values if not specified
      loginCallback
    );
  }

</script>

@endsection