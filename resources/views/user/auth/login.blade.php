@extends('user.layout.auth')

@section('content')

<div class="full-page-bg" style="background-image: url('http://192.168.1.200/ailbaz_server/public/{{Setting::get('user_backgruond_photo')}}');">
    <div class="log-overlay"></div>
    <div class="full-page-bg-inner">
        <div class="row no-margin">
            <div class="col-md-6 log-left">
                <span class="login-logo" style="background: none"><img src="{{url('/').Setting::get('site_logo', asset('logo-black.png'))}}" style="height: 120px;border-radius: 50%;border: white solid 2px"></span>
                @if(app()->getLocale()=="en")
                <h2>{!!Setting::get('big_title_en',asset(''))!!}</h2>
                <p>{!!Setting::get('small_title_en',asset(''))!!}</p>
                @else
                    <h2>{!!Setting::get('big_title_ar',asset(''))!!}</h2>
                    <p>{!!Setting::get('small_title_ar',asset(''))!!}</p>
                @endif
            </div>
            <div class="col-md-6 log-right">
                <div class="login-box-outer">
                <div class="login-box row no-margin">
                    <div class="col-md-12">
                        <a class="log-blk-btn" href="{{url('register')}}">@lang('user.create_new_account')</a>
                        <h3>@lang('user.login')</h3>
                    </div>
                    <form  role="form" method="POST" action="{{ url('/login') }}">
                    {{ csrf_field() }}
                        <div class="col-md-12">
                             <input id="email" type="email" class="form-control" placeholder="@lang('user.email_address')" name="email" value="{{ old('email') }}"  autofocus>

                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>


{{--                        <div class="col-md-12">--}}
{{--                            <input id="mobile" type="int" class="form-control" placeholder="@lang('user.mobile')" name="mobile" value="{{ old('mobile') }}" required autofocus>--}}

{{--                            @if ($errors->has('mobile'))--}}
{{--                                <span class="help-block">--}}
{{--                                    <strong>{{ $errors->first('mobile') }}</strong>--}}
{{--                                </span>--}}
{{--                            @endif--}}
{{--                        </div>--}}


                        
                        <div class="col-md-12">
                            <input id="password" type="password" class="form-control" placeholder="@lang('user.pass')" name="password" required>

                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="col-md-12">
                            <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : ''}}><span> @lang('user.remember_me')</span>
                        </div>
                       
                        <div class="col-md-12">
                            <button type="submit" class="log-teal-btn">@lang('user.login')</button>
                        </div>
                    </form>
{{--                    --}}
{{--                    @if(Setting::get('social_login', 0) == 1)--}}
{{--                    <div class="col-md-12">--}}
{{--                        <a href="{{ url('/auth/facebook') }}"><button type="submit" class="log-teal-btn fb"><i class="fa fa-facebook"></i>@lang('user.login_with_facebook')</button></a>--}}
{{--                    </div>  --}}
{{--                    <div class="col-md-12">--}}
{{--                        <a href="{{ url('/auth/google') }}"><button type="submit" class="log-teal-btn gp"><i class="fa fa-google-plus"></i>@lang('user.login_with_googel+')</button></a>--}}
{{--                    </div>--}}
{{--                    @endif--}}

                    <div class="col-md-12">
                        <p class="helper"> <a href="{{ url('/password/reset') }}">@lang('user.forget_pass')</a></p>   
                        <p class="helper"> <a href="{{ url('/') }}">@lang('user.back')</a></p>
                    </div>
                </div>


                <div class="log-copy"><p class="no-margin">{{ Setting::get('site_copyright', '&copy; '.date('Y').' Appoets') }}</p></div></div>
            </div>
        </div>
    </div>
</div>
@endsection