@extends('user.layout.app')

@section('content')
    <div class="row white-section no-margin">
        <div class="container">
            <div class="col-md-6 img-block text-center">
                <img style="width: 650px;height: 400px;"  src="{{ asset($services->image) }}">
            </div>
            <div class="col-md-6 content-block">
                <h3><label>Service Name: </label> {{ $services->name }}.</h3>
                <h3><label>Base Price: </label> {{ currency($services->fixed)}}.</h3>
                <h3><label>Base Distance: </label> {{ distance($services->distance) }}.</h3>
                <h3><label>Unit Time Pricing: </label> {{ $services->waiting }}.</h3>
                <h3><label>Unit Distance Price: </label> {{ currency($services->price) }}.</h3>
                <h3><label>Base Waiting price: </label> {{ currency($services->minute) }}.</h3>
                <h3><label> Waiting Per Minute price: </label> {{  currency($services->min_wait_price) }}.</h3>
                <h3><label>Price Calculation: </label> @lang('servicetypes.'.$services->calculator).</h3>
                <h3><label>Description: </label> {{ $services->description }}.</h3>
            </div>
        </div>
    </div>
@endsection