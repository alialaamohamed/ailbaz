@extends('admin.layout.base')

@section('title', 'Service Types ')

@section('content')
<div class="content-area py-1">
    <div class="container-fluid">
        <div class="box box-block bg-white">
            <h5 class="mb-1">@lang('admin.Service Types')</h5>
            <button class="btn btn-success pull-right Get_Modal" style="margin-left: 1em;" data-toggle="modal" data-target="#New"><i class="fa fa-plus"></i> @lang('admin.New')</button>
            <table class="table table-striped table-bordered dataTable" id="table-2">
                <thead>
                    <tr>
                        <th>@lang('admin.ID')</th>
                        <th>@lang('admin.Service Name')</th>
                        <th>@lang('admin.Base Price')</th>
                        <th>@lang('admin.Base Distance')</th>
                        <th>@lang('admin.Base Waiting')</th>
                        <th>@lang('admin.Distance Price')</th>
                        <th>@lang('admin.Time Price')</th>
                        <th>@lang('admin.Waiting Price')</th>
                        <th>@lang('admin.Price Calculation')</th>
                        <th>@lang('admin.Service Image')</th>
                        <th>@lang('admin.Status')</th>
                        <th>@lang('admin.Action')</th>
                        <th>@lang('admin.Action')</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($services as $index => $service)
                    <tr>
                        <td>{{ $index + 1 }}</td>
                        <td>{{ $service->name }}</td>
                        <td>{{ currency($service->fixed) }}</td>
                        <td>{{ distance($service->distance) }}</td>
                        <td>{{ $service->waiting }}</td>
                        <td>{{ currency($service->price) }}</td>
                        <td>{{ currency($service->minute) }}</td>
                        <td>{{ currency($service->min_wait_price) }}</td>
                        <td>@lang('servicetypes.'.$service->calculator)</td>
                        <td>
                            @if($service->image) 
                                <img src="{{url('/').$service->image}}" style="height: 50px" >
                            @else
                                N/A
                            @endif
                        </td>
                        <td>
                            <div class="col-xs-6">
                                <input id="stripe_check " onchange="changeStatus($service->id)" {{$service->status?'checked':''}} data-id="{{$service->id}}" data-model="ServiceType" type="checkbox" class="js-switch Change_Status" data-color="#43b968">
                            </div>
                        </td>
                        <td>
                            <div class="input-group-btn">
                                <button type="button" 
                                    class="btn btn-info dropdown-toggle"
                                    data-toggle="dropdown">@lang('admin.Action')
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a class="btn btn-default Get_Modal" data-id="{{$service->id}}" data-toggle="modal" data-target="#Edit"><i class="fa fa-pencil"></i> @lang('admin.Edit')</a>
                                    </li>
                                    <li>
                                        <form action="{{ route('admin.service.destroy', $service->id) }}" method="POST">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="_method" value="DELETE">
                                            <button class="btn btn-default look-a-like" onclick="return confirm('Are you sure?')"><i class="fa fa-trash"></i> @lang('admin.Delete')</button>
                                        </form>
                                    </li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th>@lang('admin.ID')</th>
                        <th>@lang('admin.Service Name')</th>
                        <th>@lang('admin.Base Price')</th>
                        <th>@lang('admin.Base Distance')</th>
                        <th>@lang('admin.Base Waiting')</th>
                        <th>@lang('admin.Distance Price')</th>
                        <th>@lang('admin.Time Price')</th>
                        <th>@lang('admin.Waiting Price')</th>
                        <th>@lang('admin.Price Calculation')</th>
                        <th>@lang('admin.Service Image')</th>
                        <th>@lang('admin.Status')</th>
                        <th>@lang('admin.Action')</th>
                        <th>@lang('admin.Action')</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
<div id="Edit" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
    <!-- Modal content-->
        <div class="modal-content">
            <form class="form-horizontal" action="{{route('admin.service.index')}}" method="post" enctype="multipart/form-data" role="form">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">@lang('admin.Edit Service Type')</h4>
                </div>
                <div class="modal-body">
                    {{csrf_field()}}
                    <input type="hidden" name="id">
                    <div class="form-group row">
                        <div class="col-xs-3">
                            <label for="name" class="col-xs-12 col-form-label">@lang('admin.Service Name')</label>
                        </div>
                        <div class="col-xs-9">
                            <input class="form-control" type="text" value="{{ old('name') }}" name="name" required id="name" placeholder="@lang('admin.Service Name')">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-xs-3">
                            <label for="picture" class="col-xs-12 col-form-label">@lang('admin.Service Image')</label>
                        </div>
                        <div class="col-xs-5">
                            <input type="file" accept="image/*" name="image" class="dropify form-control-file" id="picture" aria-describedby="fileHelp">
                        </div>
                        <div class="col-xs-4">
                            <img class="img-responsive" src="" alt="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-xs-3">
                            <label for="fixed" class="col-xs-12 col-form-label">@lang('admin.Base Price') ({{ currency() }})</label>
                        </div>
                        <div class="col-xs-9">
                            <input class="form-control" type="text" value="{{ old('fixed') }}" name="fixed" required id="fixed" placeholder="@lang('admin.Base Price')">
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-xs-3">
                            <label for="distance" class="col-xs-12 col-form-label">@lang('admin.Base Distance') ({{ distance() }})</label>
                        </div>
                        <div class="col-xs-9">
                            <input class="form-control" type="text" value="{{ old('distance') }}" name="distance" required id="distance" placeholder="@lang('admin.Base Distance')">
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-xs-3">
                            <label for="minute" class="col-xs-12 col-form-label">@lang('admin.Unit Time Pricing') (@lang('admin.For Rental amount per hour') / 60) ({{ currency() }})</label>
                        </div>
                        <div class="col-xs-9">
                            <input class="form-control" type="text" value="{{ old('minute') }}" name="minute" required id="minute" placeholder="@lang('admin.Unit Time Pricing')">
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-xs-3">
                            <label for="price" class="col-xs-12 col-form-label">@lang('admin.Unit Distance Price') ({{ distance() }})</label>
                        </div>
                        <div class="col-xs-9">
                            <input class="form-control" type="text" value="{{ old('price') }}" name="price" required id="price" placeholder="@lang('admin.Unit Distance Price')">
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-xs-3">
                            <label for="waiting" class="col-xs-12 col-form-label">@lang('admin.Base Waiting')</label>
                        </div>
                        <div class="col-xs-9">
                            <input class="form-control" type="text" value="{{ old('waiting') }}" name="waiting" required id="waiting" placeholder="@lang('admin.Base Waiting') ({{ currency() }})">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-xs-3">
                            <label for="min_wait_price" class="col-xs-12 col-form-label">@lang('admin.Waiting Per Minute') ({{ currency() }})</label>
                        </div>
                        <div class="col-xs-9">
                            <input class="form-control" type="text" value="{{ old('fixed') }}" name="min_wait_price" required id="min_wait_price" placeholder="@lang('admin.Waiting Per Minute')">
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-xs-3">
                            <label for="calculator" class="col-xs-12 col-form-label">@lang('admin.Pricing Logic')</label>
                        </div>
                        <div class="col-xs-9">
                            <select class="form-control" id="calculator" name="calculator">
                                <option value="MIN">@lang('servicetypes.MIN')</option>
                                <option value="HOUR">@lang('servicetypes.HOUR')</option>
                                <option value="DISTANCE">@lang('servicetypes.DISTANCE')</option>
                                <option value="DISTANCEMIN">@lang('servicetypes.DISTANCEMIN')</option>
                                <option value="DISTANCEHOUR">@lang('servicetypes.DISTANCEHOUR')</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-xs-3">
                            <label for="description" class="col-xs-12 col-form-label">@lang('admin.Description')</label>
                        </div>
                        <div class="col-xs-9">
                            <textarea class="form-control" type="floa" value="{{ old('description') }}" name="description" required id="description" placeholder="@lang('admin.Description')" rows="4"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">@lang('admin.Close')</button>
                    <button type="submit" class="btn btn-primary">@lang('admin.Update Service Type')</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div id="New" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
    <!-- Modal content-->
        <div class="modal-content">
            <form class="form-horizontal" action="{{route('admin.service.store')}}" method="post" enctype="multipart/form-data" role="form">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">@lang('admin.New')</h4>
                </div>
                <div class="modal-body">
                    {{csrf_field()}}
                    <div class="form-group row">
                        <div class="col-xs-3">
                            <label for="name" class="col-xs-12 col-form-label">@lang('admin.Service Name')</label>
                        </div>
                        <div class="col-xs-9">
                            <input class="form-control" type="text" value="{{ old('name') }}" name="name" required id="name" placeholder="@lang('admin.Service Name')">
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-xs-3">
                            <label for="picture" class="col-xs-12 col-form-label">@lang('admin.Service Image')</label>
                        </div>
                        <div class="col-xs-9">
                            <input type="file" accept="image/*" name="image" class="dropify form-control-file" id="picture" aria-describedby="fileHelp">
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-xs-3">
                            <label for="fixed" class="col-xs-12 col-form-label">@lang('admin.Base Price') ({{ currency() }})</label>
                        </div>
                        <div class="col-xs-9">
                            <input class="form-control" type="text" value="{{ old('fixed') }}" name="fixed" required id="fixed" placeholder="@lang('admin.Base Price')">
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-xs-3">
                            <label for="distance" class="col-xs-12 col-form-label">@lang('admin.Base Distance') ({{ distance() }})</label>
                        </div>
                        <div class="col-xs-9">
                            <input class="form-control" type="text" value="{{ old('distance') }}" name="distance" required id="distance" placeholder="@lang('admin.Base Distance')">
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-xs-3">
                            <label for="minute" class="col-xs-12 col-form-label">@lang('admin.Unit Time Pricing') (@lang('admin.For Rental amount per hour') / 60) ({{ currency() }})</label>
                        </div>
                        <div class="col-xs-9">
                            <input class="form-control" type="text" value="{{ old('minute') }}" name="minute" required id="minute" placeholder="@lang('admin.Unit Time Pricing')">
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-xs-3">
                            <label for="price" class="col-xs-12 col-form-label">@lang('admin.Unit Distance Price') ({{ distance() }})</label>
                        </div>
                        <div class="col-xs-9">
                            <input class="form-control" type="text" value="{{ old('price') }}" name="price" required id="price" placeholder="@lang('admin.Unit Distance Price')">
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-xs-3">
                            <label for="waiting" class="col-xs-12 col-form-label">@lang('admin.Base Waiting')</label>
                        </div>
                        <div class="col-xs-9">
                            <input class="form-control" type="text" value="{{ old('waiting') }}" name="waiting" required id="waiting" placeholder="@lang('admin.Base Waiting') ({{ currency() }})">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-xs-3">
                            <label for="min_wait_price" class="col-xs-12 col-form-label">@lang('admin.Waiting Per Minute') ({{ currency() }})</label>
                        </div>
                        <div class="col-xs-9">
                            <input class="form-control" type="text" value="{{ old('fixed') }}" name="min_wait_price" required id="min_wait_price" placeholder="@lang('admin.Waiting Per Minute')">
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-xs-3">
                            <label for="calculator" class="col-xs-12 col-form-label">@lang('admin.Pricing Logic')</label>
                        </div>
                        <div class="col-xs-9">
                            <select class="form-control" id="calculator" name="calculator">
                                <option value="MIN">@lang('servicetypes.MIN')</option>
                                <option value="HOUR">@lang('servicetypes.HOUR')</option>
                                <option value="DISTANCE">@lang('servicetypes.DISTANCE')</option>
                                <option value="DISTANCEMIN">@lang('servicetypes.DISTANCEMIN')</option>
                                <option value="DISTANCEHOUR">@lang('servicetypes.DISTANCEHOUR')</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-xs-3">
                            <label for="description" class="col-xs-12 col-form-label">@lang('admin.Description')</label>
                        </div>
                        <div class="col-xs-9">
                            <textarea class="form-control" type="floa" value="{{ old('description') }}" name="description" required id="description" placeholder="@lang('admin.Description')" rows="4"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">@lang('admin.Close')</button>
                    <button type="submit" class="btn btn-success">@lang('admin.Create Service Type')</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script>
$('body').on('click','.Get_Modal',function(){
    var Target_Modal = $(this).data('target');
    if($(this).data('id')){
        $.ajax({
            type:'get',
            url:"{{route('admin.service.index')}}/"+$(this).data('id'),
            success:function(data){
                $.each(data,function(key,value){
                    $(Target_Modal).find('input[name="'+key+'"]').not('input[type="file"]').val(value); 
                    $(Target_Modal).find('textarea[name="'+key+'"]').text(value); 
                    $(Target_Modal).find('select[name="'+key+'"]').val(value).change(); 
                    $(Target_Modal).find('input[name="'+key+'"][type="file"]').closest('.row').find('img').attr('src',value); 
                });
            }
        });
    }
})
    function changeStatus($id){
        $.ajax({
            type:'post',
            data:{service_id:$id , status: $("#stripe_check").val()},
            url:"{{'admin/service/changeStatus'}}",
            success:function(data){
                alert(data);
            }
        });
    }
</script>
@endsection