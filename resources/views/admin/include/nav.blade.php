<div class="site-sidebar">
	<div class="custom-scroll custom-scroll-light">
		<ul class="sidebar-menu">
			<li class="menu-title">@lang('admin.Admin Dashboard')</li>
			<li>
				<a href="{{ route('admin.dashboard') }}" class="waves-effect waves-light">
					<span class="s-icon"><i class="ti-anchor"></i></span>
					<span class="s-text">@lang('admin.Dashboard')</span>
				</a>
			</li>
			<li>
				<a href="{{ route('admin.dispatcher.index') }}" class="waves-effect waves-light">
					<span class="s-icon"><i class="ti-target"></i></span>
					<span class="s-text">@lang('admin.Dispatcher Panel')</span>
				</a>
			</li>

			<li>
				<a href="{{ route('admin.heatmap') }}" class="waves-effect waves-light">
					<span class="s-icon"><i class="ti-map"></i></span>
					<span class="s-text">@lang('admin.Heat Map')</span>
				</a>
			</li>
			<li class="menu-title">@lang('admin.Transportation')</li>
			<li>
				<a href="{{ route('admin.transtype.index') }}" class="waves-effect  waves-light">
					<span class="s-icon"><i class="ti-infinite"></i></span>
					<span class="s-text">@lang('admin.Transportation Types')</span>
				</a>
			</li>
			<li>
				<a href="{{ route('admin.service.index') }}" class="waves-effect  waves-light">
					<span class="s-icon"><i class="ti-view-grid"></i></span>
					<span class="s-text">@lang('admin.Service Types')</span>
				</a>
			</li>
			<li>
				<a href="{{ route('admin.carclass.index') }}" class="waves-effect  waves-light">
					<span class="s-icon"><i class="ti-car"></i></span>
					<span class="s-text">@lang('admin.Cars')</span>
				</a>
			</li>
			<li>
				<a href="{{ route('admin.carmodel.index') }}" class="waves-effect  waves-light">
					<span class="s-icon"><i class="ti-infinite"></i></span>
					<span class="s-text">@lang('admin.Car Models')</span>
				</a>
			</li>
			<li class="menu-title">@lang('admin.Members')</li>
			<li class="with-sub">
				<a href="#" class="waves-effect  waves-light">
					<span class="s-caret"><i class="fa fa-angle-down"></i></span>
					<span class="s-icon"><i class="ti-crown"></i></span>
					<span class="s-text">@lang('admin.Users')</span>
				</a>
				<ul>
					<li><a href="{{ route('admin.user.index') }}">@lang('admin.List Users')</a></li>
					<li><a href="{{ route('admin.user.create') }}">@lang('admin.Add New User')</a></li>
				</ul>
			</li>
			<li class="with-sub">
				<a href="#" class="waves-effect  waves-light">
					<span class="s-caret"><i class="fa fa-angle-down"></i></span>
					<span class="s-icon"><i class="ti-car"></i></span>
					<span class="s-text">@lang('admin.Providers')</span>
				</a>
				<ul>
					<li><a href="{{ route('admin.provider.index') }}">@lang('admin.List Providers')</a></li>
					<li><a href="{{ route('admin.provider.create') }}">@lang('admin.Add New Provider')</a></li>
				</ul>
			</li>
			<li class="with-sub">
				<a href="#" class="waves-effect  waves-light">
					<span class="s-caret"><i class="fa fa-angle-down"></i></span>
					<span class="s-icon"><i class="ti-crown"></i></span>
					<span class="s-text">@lang('admin.Dispatcher')</span>
				</a>
				<ul>
					<li><a href="{{ route('admin.dispatch-manager.index') }}">@lang('admin.List Dispatcher')</a></li>
					<li><a href="{{ route('admin.dispatch-manager.create') }}">@lang('admin.Add New Dispatcher')</a></li>
				</ul>
			</li>
			<li class="with-sub">
				<a href="#" class="waves-effect  waves-light">
					<span class="s-caret"><i class="fa fa-angle-down"></i></span>
					<span class="s-icon"><i class="ti-crown"></i></span>
					<span class="s-text">@lang('admin.Fleet Owner')</span>
				</a>
				<ul>
					<li><a href="{{ route('admin.fleet.index') }}">@lang('admin.List Fleets')</a></li>
					<li><a href="{{ route('admin.fleet.create') }}">@lang('admin.Add New Fleet Owner')</a></li>
				</ul>
			</li>
			<li class="with-sub">
				<a href="#" class="waves-effect  waves-light">
					<span class="s-caret"><i class="fa fa-angle-down"></i></span>
					<span class="s-icon"><i class="ti-crown"></i></span>
					<span class="s-text">@lang('admin.Account Manager')</span>
				</a>
				<ul>
					<li><a href="{{ route('admin.account-manager.index') }}">@lang('admin.List Account Managers')</a></li>
					<li><a href="{{ route('admin.account-manager.create') }}">@lang('admin.Add New Account Manager')</a></li>
				</ul>
			</li>
			<li class="menu-title">الإيميلات</li>
			<li class="with-sub">
				<a href="#" class="waves-effect  waves-light">
					<span class="s-caret"><i class="fa fa-angle-down"></i></span>
					<span class="s-icon"><i class="ti-crown"></i></span>
					<span class="s-text">@lang('admin.Statements')</span>
				</a>
				<ul>
					<li><a href="{{ route('admin.ride.statement') }}">@lang('admin.Overall Ride Statments')</a></li>
					<li><a href="{{ route('admin.ride.statement.provider') }}">@lang('admin.Provider Statement')</a></li>
					<li><a href="{{ route('admin.ride.statement.today') }}">@lang('admin.Daily Statement')</a></li>
					<li><a href="{{ route('admin.ride.statement.monthly') }}">@lang('admin.Monthly Statement')</a></li>
					<li><a href="{{ route('admin.ride.statement.yearly') }}">@lang('admin.Yearly Statement')</a></li>
				</ul>
			</li>

			<li class="menu-title">@lang('admin.Details')</li>
			<li>
				<a href="{{ route('admin.map.index') }}" class="waves-effect waves-light">
					<span class="s-icon"><i class="ti-map-alt"></i></span>
					<span class="s-text">@lang('admin.Map')</span>
				</a>
			</li>
			<li class="with-sub">
				<a href="#" class="waves-effect  waves-light">
					<span class="s-caret"><i class="fa fa-angle-down"></i></span>
					<span class="s-icon"><i class="ti-view-grid"></i></span>
					<span class="s-text">@lang('admin.Ratings') &amp; @lang('admin.Reviews')</span>
				</a>
				<ul>
					<li><a href="{{ route('admin.user.review') }}">@lang('admin.User Ratings')</a></li>
					<li><a href="{{ route('admin.provider.review') }}">@lang('admin.Provider Ratings')</a></li>
				</ul>
			</li>
			<li class="menu-title">@lang('admin.Requests')</li>
			<li>
				<a href="{{ route('admin.requests.index') }}" class="waves-effect  waves-light">
					<span class="s-icon"><i class="ti-infinite"></i></span>
					<span class="s-text">@lang('admin.Request History')</span>
				</a>
			</li>
			<li>
				<a href="{{ route('admin.requests.scheduled') }}" class="waves-effect  waves-light">
					<span class="s-icon"><i class="ti-palette"></i></span>
					<span class="s-text">@lang('admin.Scheduled Rides')</span>
				</a>
			</li>
			<li class="menu-title">@lang('admin.General')</li>

			<li class="with-sub">
				<a href="#" class="waves-effect  waves-light">
					<span class="s-caret"><i class="fa fa-angle-down"></i></span>
					<span class="s-icon"><i class="ti-layout-tab"></i></span>
					<span class="s-text">@lang('admin.Documents')</span>
				</a>
				<ul>
					<li><a href="{{ route('admin.document.index') }}">@lang('admin.List Documents')</a></li>
					<li><a href="{{ route('admin.document.create') }}">@lang('admin.Add New Document')</a></li>
				</ul>
			</li>

			<li class="with-sub">
				<a href="#" class="waves-effect  waves-light">
					<span class="s-caret"><i class="fa fa-angle-down"></i></span>
					<span class="s-icon"><i class="ti-layout-tab"></i></span>
					<span class="s-text">@lang('admin.Promocodes')</span>
				</a>
				<ul>
					<li><a href="{{ route('admin.promocode.index') }}">@lang('admin.List Promocodes')</a></li>
					<li><a href="{{ route('admin.promocode.create') }}">@lang('admin.Add New Promocode')</a></li>
				</ul>
			</li>

			<li class="menu-title">@lang('admin.Payment Details')</li>
			<li>
				<a href="{{ route('admin.payment') }}" class="waves-effect  waves-light">
					<span class="s-icon"><i class="ti-infinite"></i></span>
					<span class="s-text">@lang('admin.Payment History')</span>
				</a>
			</li>
			<li>
				<a href="{{ route('admin.settings.payment') }}" class="waves-effect  waves-light">
					<span class="s-icon"><i class="ti-money"></i></span>
					<span class="s-text">@lang('admin.Payment Settings')</span>
				</a>
			</li>
{{--			<li class="menu-title">static</li>--}}
{{--			<li>--}}
{{--				<a href="{{ route('admin.condition_settings') }}" class="waves-effect  waves-light">--}}
{{--					<span class="s-icon"><i class="ti-settings"></i></span>--}}
{{--					<span class="s-text">Service Conditions</span>--}}
{{--				</a>--}}
			</li>
			<li class="with-sub">
				<a href="#" class="waves-effect  waves-light">
					<span class="s-caret"><i class="fa fa-angle-down"></i></span>
					<span class="s-icon"><i class="ti-layout-tab"></i></span>
					<span class="s-text">@lang('admin.Setting')</span>
				</a>
				<ul>
					<li><a href="{{ route('admin.settings') }}">@lang('admin.Site Settings')</a></li>
					<li><a href="{{ route('admin.dash_settings') }}">@lang('admin.main Settings')</a></li>
					<li><a href="{{ route('admin.condition_settings') }}">@lang('admin.Service Conditions')</a></li>
				</ul>
			</li>

{{--			<li class="menu-title">Settings</li>--}}
{{--			<li>--}}
{{--				<a href="{{ route('admin.settings') }}" class="waves-effect  waves-light">--}}
{{--					<span class="s-icon"><i class="ti-settings"></i></span>--}}
{{--					<span class="s-text">Site Settings</span>--}}
{{--				</a>--}}
{{--			</li>--}}

{{--			<li>--}}
{{--				<a href="{{ route('admin.dash_settings') }}" class="waves-effect  waves-light">--}}
{{--					<span class="s-icon"><i class="ti-settings"></i></span>--}}
{{--					<span class="s-text">main page</span>--}}
{{--				</a>--}}
{{--			</li>--}}

{{--			<li class="menu-title">Others</li>--}}
{{--			<li>--}}
{{--				<a href="{{ route('admin.privacy') }}" class="waves-effect waves-light">--}}
{{--					<span class="s-icon"><i class="ti-help"></i></span>--}}
{{--				<span class="s-text">Privacy Policy</span>--}}
{{--				</a>--}}
{{--			</li>--}}
{{--			<li>--}}
{{--				<a href="{{ route('admin.help') }}" class="waves-effect waves-light">--}}
{{--				<span class="s-icon"><i class="ti-help"></i></span>--}}
{{--				<span class="s-text">Help</span>--}}
{{--				</a>--}}
{{--			</li>--}}
{{--			<li>--}}
{{--				<a href="{{route('admin.translation') }}" class="waves-effect waves-light">--}}
{{--					<span class="s-icon"><i class="ti-smallcap"></i></span>--}}
{{--					<span class="s-text">Translations</span>--}}
{{--				</a>--}}
{{--			</li>--}}
			<li class="menu-title">@lang('admin.Account')</li>
			<li>
				<a href="{{ route('admin.profile') }}" class="waves-effect  waves-light">
					<span class="s-icon"><i class="ti-user"></i></span>
					<span class="s-text">@lang('admin.Account Settings')</span>
				</a>
			</li>
			<li>
				<a href="{{ route('admin.password') }}" class="waves-effect  waves-light">
					<span class="s-icon"><i class="ti-exchange-vertical"></i></span>
					<span class="s-text">@lang('admin.Change Password')</span>
				</a>
			</li>
			<li class="compact-hide">
				<a href="{{ url('/admin/logout') }}"
                            onclick="event.preventDefault();
                                     document.getElementById('logout-form').submit();">
					<span class="s-icon"><i class="ti-power-off"></i></span>
					<span class="s-text">@lang('admin.Logout')</span>
                </a>

                <form id="logout-form" action="{{ url('/admin/logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
			</li>

		</ul>
	</div>
</div>