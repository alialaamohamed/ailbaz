@extends('user.layout.app')

@section('content')

<div class="banner row no-margin" style="
        background-image: url('{{url('/').Setting::get('First_site_photo', asset('asset/img/banner-bg.jpg'))}}');
        height: 100vh;
        ">
    <div class="banner-overlay"></div>
    <div class="container">

        @if(app()->getLocale()=="en")
        <div class="col-md-8">
            <h2 class="banner-head">
                {!! (Setting::get("text_first_en",'<span class="strong">Get there</span><br> Your day belongs to you')) !!}
            </h2>
            <a style=" color: aliceblue;" class="menu-btn" href="{{url('/provider/register')}}">Be a Companion</a>
        </div>
        @else
        <div class="col-md-8">
            <h2 class="banner-head">{!! (Setting::get("text_first",'<span class="strong">الباز</span><br>يرحب بكم')) !!}</h2>
            <a  class="menu-btn" href="{{url('/provider/register')}}">@lang('user.become_a_driv')</a>
        </div>
        @endif

        @if(app()->getLocale()=="en")
       <div class="col-md-4" style="background-color: rgba(255,255,255,0);">
            <div class="banner-form"  style="background-color: rgba(255,255,255,0.23); color: #fff;">

                <div class="row no-margin fields">
                    <div class="left">
                        <img src="{{ asset('asset/img/5.png') }}">
                    </div>
                    <div class="right">
                        <a href="{{ url('register') }}">

                            <h3>Sign up to Ride</h3>
                            <h5>SIGN UP <i class="fa fa-chevron-right"></i></h5>
                        </a>
                    </div>
                </div>
                <div class="row no-margin fields">
                    <div class="left">
                        <img src="{{ asset('asset/img/6.png') }}">
                    </div>
                    <div class="right">
                        <a href="{{url('/provider/register')}}" style="color: #fff">
                            <h3>Sign up to Drive</h3>
                            <h5>SIGN UP <i class="fa fa-chevron-right"></i></h5>
                        </a>
                    </div>
                </div>
                <p class="note-or">Or <a href="{{ url('/provider/login') }}">sign in</a> with your rider account.</p>
            </div>
        </div>
    </div>
        @else
    <div class="col-md-4" style="background-color: rgba(255,255,255,0);">
        <div class="banner-form"  style="background-color: rgba(255,255,255,0.23); color: #fff;">

            <div class="row no-margin fields">
                <div class="left">
                    <img src="{{ asset('asset/img/5.png') }}">
                </div>
                <div class="right">
                    <a href="{{ url('register') }}">
                        <h3>تسجيل عميل جديد</h3>
                        <h5><i class="fa fa-chevron-left"></i>سجل</h5>
                    </a>
                </div>
            </div>
            <div class="row no-margin fields">
                <div class="left">
                    <img src="{{ asset('asset/img/6.png') }}">
                </div>
                <div class="right">
                    <a href="{{url('/provider/register')}}" style="color: #fff">
                        <h3>تسجيل سائق جديد</h3>
                        <h5><i class="fa fa-chevron-left"></i>سجل</h5>
                    </a>
                </div>
            </div>
            <p class="note-or"> أو <a href="{{ url('/provider/login') }}"> تسجيل الدخول</a> باستخدام حساب السائق الخاص بك. </p>
        </div>
    </div>
</div>
    @endif
</div>
@if(app()->getLocale()=="en")
<div class="row white-section no-margin" >
    <div class="container-fluid">
        <section class="why text-center">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="title">
                            <h2> {{Setting::get('about_title_en','Why choose us')}}  </h2>
                            <h6>{{Setting::get('about_small_title_en','Best services in the city')}} </h6>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="service" style="
                        background-color: rgba(248,248,249,1);
                        padding: 15px;
                        border-radius: 15px;
                        line-height: 25px;
                        margin-bottom: 5px;
">
                            <img src="{{url('/').Setting::get('First_about_photo',asset(''))}}" style="width: 100px;height: 100px">
{{--                            <i class="fab fa-accessible-icon " style="font-size: 5em; color:#ff9000; margin-bottom: 10px"></i>--}}
                            <p><strong>
                                    {!!Setting::get('first_name_en',asset(''))!!}
                                </strong><br>
                                {!!Setting::get('first_details_en',asset(''))!!}
                            </p>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="service" style="
                        background-color: rgba(248,248,249,1);

                        padding: 15px;
                        border-radius: 15px;
                        line-height: 25px;
                        margin-bottom: 5px;
">

                            <img src="{{url('/').Setting::get('Second_about_photo',asset(''))}}" style="width: 100px;height: 100px">

{{--                            <i class="fab fa-accessible-icon " style="font-size: 5em; color:#ff9000; margin-bottom: 10px"></i>--}}
                            <p><strong>
                                    {!!Setting::get('second_name_en',asset(''))!!}
                                </strong><br>
                                {!!Setting::get('second_details_en',asset(''))!!}
                            </p>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="service" style="
                        background-color: rgba(248,248,249,1);
                        padding: 15px;
                        border-radius: 15px;
                        line-height: 25px;
                        margin-bottom: 5px;
">
                            <img src="{{url('/').Setting::get('Third_about_photo',asset(''))}}" style="width: 100px;height: 100px">
{{--                            <i class="fab fa-accessible-icon " style="font-size: 5em; color:#ff9000; margin-bottom: 10px"></i>--}}
                            <p><strong>
                                    {!!Setting::get('third_name_en',asset(''))!!}
                                </strong><br>
                                {!!Setting::get('third_details_en',asset(''))!!}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

@else
<div class="row white-section no-margin" >
    <div class="container-fluid">
        <section class="why text-center">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="title">
                            <h2> {{Setting::get('about_title',' لماذا أخترتنا')}}  </h2>
                            <h6>{{Setting::get('about_small_title','أفضل الخدمات في المدينة')}} </h6>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="service" style="
                        background-color: rgba(248,248,249,1);
                        padding: 15px;
                        border-radius: 15px;
                        line-height: 25px;
                        margin-bottom: 5px;
">
                            <img src="{{url('/').Setting::get('First_about_photo',asset(''))}}" style="width: 100px;height: 100px">
                            {{--                            <i class="fab fa-accessible-icon " style="font-size: 5em; color:#ff9000; margin-bottom: 10px"></i>--}}
                            <p><strong>
                                    {!!Setting::get('first_name',asset(''))!!}
                                </strong><br>
                                {!!Setting::get('first_details',asset(''))!!}
                            </p>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="service" style="
                        background-color: rgba(248,248,249,1);

                        padding: 15px;
                        border-radius: 15px;
                        line-height: 25px;
                        margin-bottom: 5px;
">

                            <img src="{{url('/').Setting::get('Second_about_photo',asset(''))}}" style="width: 100px;height: 100px">

                            {{--                            <i class="fab fa-accessible-icon " style="font-size: 5em; color:#ff9000; margin-bottom: 10px"></i>--}}
                            <p><strong>
                                    {!!Setting::get('second_name',asset(''))!!}
                                </strong><br>
                                {!!Setting::get('second_details',asset(''))!!}
                            </p>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="service" style="
                        background-color: rgba(248,248,249,1);
                        padding: 15px;
                        border-radius: 15px;
                        line-height: 25px;
                        margin-bottom: 5px;
">
                            <img src="{{url('/').Setting::get('Third_about_photo',asset(''))}}" style="width: 100px;height: 100px">
                            {{--                            <i class="fab fa-accessible-icon " style="font-size: 5em; color:#ff9000; margin-bottom: 10px"></i>--}}
                            <p><strong>
                                    {!!Setting::get('third_name',asset(''))!!}
                                </strong><br>
                                {!!Setting::get('third_details',asset(''))!!}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

@endif


<div class="row">
    <div class="container-fluid">
        <img src="{{ url('/').Setting::get('Second_site_photo',  asset('asset/img/aa.png')) }}" alt="Image" style="max-width: 100%;left: 0" class="img-fluid">
    </div>
</div>


<?php
        $i=3;
foreach (App\Box::all() as $item) {
    if($i % 2){

?>
@if(app()->getLocale()=='en')
<div class="row white-section no-margin">
    <div class="container">
        <div class="col-md-6 img-block text-center">
            <img style="width: 500px;height: 400px" src="{{asset($item->photo)}}">
        </div>
        <div class="col-md-6 content-block">
            <h2>{!!$item->title_en!!}</h2>
            <div class="title-divider"></div>
            <p>{!! $item->details_en !!} </p>
            <a target="_blank" class="content-more" href="{{$item->link}}">@lang('user.m_r_r') <i class="fa fa-chevron-right"></i></a>
        </div>
    </div>
</div>
@else
<div class="row white-section no-margin">
    <div class="container">
        <div class="col-md-6 img-block text-center">
            <img style="width: 500px;height: 400px" src="{{asset($item->photo)}}">
        </div>
        <div class="col-md-6 content-block">
            <h2>{!!$item->title!!}</h2>
            <div class="title-divider"></div>
            <p>{!! $item->details !!} </p>
            <a target="_blank" class="content-more" href="{{$item->link}}">@lang('user.m_r_r') <i class="fa fa-chevron-right"></i></a>
        </div>
    </div>
</div>
@endif


<?php }else{?>

@if(app()->getLocale()=='en')
<div class="row gray-section no-margin">
    <div class="container">
        <div class="col-md-6 content-block">
            <h2>{!! $item->title_en!!}</h2>
            <div class="title-divider"></div>
            <p>{!! $item->details_en !!} </p>
            <a target="_blank" class="content-more" href="{{$item->link}}">@lang('user.m_r_r')  <i class="fa fa-chevron-right"></i></a>
        </div>
        <div class="col-md-6 img-block text-center">
            <img style="width: 500px;height: 400px" src="{{asset($item->photo)}}">
        </div>
    </div>
</div>


@else
<div class="row gray-section no-margin">
    <div class="container">
        <div class="col-md-6 content-block">
            <h2>{!! $item->title!!}</h2>
            <div class="title-divider"></div>
            <p>{!! $item->details !!} </p>
            <a target="_blank" class="content-more" href="{{$item->link}}">@lang('user.m_r_r')  <i class="fa fa-chevron-right"></i></a>
        </div>
        <div class="col-md-6 img-block text-center">
            <img style="width: 500px;height: 400px" src="{{asset($item->photo)}}">
        </div>
    </div>
</div>
@endif


<?php } $i++;}?>

<div class="row find-city no-margin col-lg-12">
    @if(app()->getLocale()=='en')
    <div class="container col-lg-4" style="float: left">
        <h2>{!! Setting::get('search_title_en','AilBaz') !!}</h2>
        <form>
            <div class="input-group find-form">
                <input  type="text" class="form-control" placeholder="@lang('user.srch')" >
                <span class="input-group-addon">
                    <button type="submit">
                        <i class="fa fa-arrow-right"></i>
                    </button>  
                </span>
            </div>
        </form>
    </div>
        @else
        <div class="container col-lg-4" style="float: right">
            <h2>{!! Setting::get('search_title','الباز') !!}</h2>
            <form>
                <div class="input-group find-form">
                    <input style="text-align: end;" type="text" class="form-control" placeholder="@lang('user.srch')" >

                        <span class="input-group-addon">
                    <button type="submit">
                        <i class="fa fa-arrow-right"></i>
                    </button>

                </span>
                </div>
            </form>
        </div>
    @endif
</div>

<div class="footer-city row no-margin" style="background-image: url({{url('/').Setting::get('footer_photo', asset('asset/img/footer-city.png')) }});"></div>
@endsection