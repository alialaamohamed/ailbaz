<?php
return [
    "Admin Dashboard"=>"لوحة القيادة الادارية",
    "Dashboard"=>"لوحة القيادة",
    "Dispatcher Panel"=>"لوحة المرسلات",
    "Heat Map"=>"خريطة الحرارة",
    "Transportation"=>"وسائل النقل",
    "Transportation Types"=>"أنواع النقل",
    "Service Types"=>"أنواع الخدمات",
    "Cars"=>"السيارات",
    "Car Models"=>"موديل السياره",
    "Members"=>"الافراد",
    "Users"=>"المستخدمين",
    "List Users"=>"قائمه المستخدمين",
    "Add New User"=>"اضافه مستخدم جديد",
    "Providers"=>"مقدمي",
    "List Providers"=>"قائمة السائقين المتقدمين",
    "Add New Provider"=>"إضافة سائق جديد",
    "Dispatcher"=>"المرسلات",
    "List Dispatcher"=>"قائمه المرسلات",
    "Add New Dispatcher"=>"اضافه مرسل جديد",
    "Fleet Owner"=>"مالك الشركة",
    "List Fleets"=>"قائمة الشركات",
    "Add New Fleet Owner"=>"أضف  شركة جديد",
    "Account Manager"=>"إدارة حساب المستخدم",
    "List Account Managers"=>"قائمة مديري الحسابات",
    "Add New Account Manager"=>"إضافة مدير حساب جديد",
    "Statements"=>"صياغات",
    "Overall Ride Statments"=>"البيانات العمومية",
    "Provider Statement"=>"بيان السائق",
    "Daily Statement"=>"بيان يومي",
    "Monthly Statement"=>"البيان الشهري",
    "Yearly Statement"=>"بيان سنوي",
    "Details"=>"التفاصيل",
    "Map"=>"خريطة",
    "Ratings"=>"التقيمات",
    "Reviews"=>"التعليقات",
    "User Ratings"=>"تقييمات المستخدم",
    "Provider Ratings"=>"تقييمات الموفر",
    "Requests"=>"الطلبات",
    "Request History"=>"الرحلات بالتاريخ",
    "Scheduled Rides"=>"الرحلات المجدولة",
    "General"=>"عام",
    "Documents"=>"مستندات",
    "List Documents"=>"قائمة المستندات",
    "Add New Document"=>"أضف وثيقة جديدة",
    "Promocodes"=>"رموز الترويجي",
    "List Promocodes"=>"قائمة البروموكود  ",
    "Payment Details"=>"بيانات الدفع",
    "Payment History"=>"تاريخ الدفع",
    "Payment Settings"=>"إعدادات الدفع",
    "Setting"=>"ضبط",
    "Site Settings"=>"إعدادات الموقع",
    "main Settings"=>"الإعدادات الرئيسية",
    "Service Conditions"=>"شروط الخدمة",
    "Account"=>"الحساب",
    "Account Settings"=>"إعدادت الحساب",
    "Change Password"=>"تغيير كلمة المرور",
    "Logout"=>"تسجيل الخروج",

    "Total No. of Rides"=>"إجمالى عدد الرحلات",
    "down from cancelled Request"=>"بانخفاض عن الطلب الملغى",
    "Revenue"=>"الإيرادات",
    "from"=>"من",
    "Rides"=>"الركوب",
    "No. of Service Types"=>"عدد أنواع الخدمات",
    "Total Cancelled Rides"=>"مجموع الرحلات الملغاة",
    "for"=>"الي",
    "User Cancelled Count"=>"عدد المستخدمين الذين الغوا الرحلة",
    "Provider Cancelled Count"=>"عدد السائقين الذين الغوا الرحلة",
    "No. of Fleets"=>"عدد الشركات",
    "No. of Scheduled Rides"=>"عدد الرحلات المحجوزة",
    "Recent Rides"=>"الركوب الاخيره",

    "View Ride Details"=>"عرض تفاصيل ركوب",
    "No Details Found"=>"لم يتم العثور على تفاصيل",

    "Ride Heatmap"=>"مكان مرتفع الطلب",
    "Drivers Rating"=>"تقييم السائقين",

    "Change Password"=>"تغيير كلمة المرور",
    "Old Password"=>"كلمة المرور قديمة",
    "New Password"=>"كلمه المرور الجديده",
    "Password Confirmation"=>"تاكيد كلمه المرور",
    "Change Password"=>"تغير كلمه المرور",

    'payment_history' => 'تاريخ الدفع',
    'request_id' => 'طلب معرف',
    'transaction_id' => 'معرف المعاملة ',
    'form' => ' من',
    'to' => 'إالى',
    'total_amount' => 'المبلغ الإجمالي',
    'payment_mode' => 'طريقة الدفع',
    'st_card_pay' => 'الشريط (مدفوعات البطاقات)',
    'payment_status' => 'حالة السداد ',
    'stripe_secret_key' => 'الشريط السري الرئيسية',
    'stripe_publishable_key' => 'شريط مفتاح للنشر',
    'cash_payments' => 'التسديد نقدا',
    'payment_settings' => 'إعدادات الدفع',
    'daily_target' => 'الهدف اليومي',
    'tax_percentage' => 'نسبة الضريبة',
    'surge_trigger_point' => 'نقطة الانطلاق',
    'surge_percentage' => 'نسبة الزيادة',
    'commission_percentage' => 'نسبة العمولة',
    'booking_id_prefix' => 'بادئة معرف الحجز',
    'currency' => 'دقة',
    'e_p' => 'الجنيه المصري',
    'us_do' => 'الدولار الأمريكي ',
    'in_ru' => 'الروبية الهندية ',
    'ku_di' => 'دينار كويتي',
    'ba_di' => 'دينار بحريني',
    'om_ri' => 'الريال العماني',
    'br_pou' => 'الجنيه البريطاني',
    'euro' => 'اليورو',
    's_f' => 'الفرنك السويسري',
    'li_di' => 'الدينار الليبي',
    'br_do' => 'دولار بروناي',
    'si_do' => 'دولار سينغافوري',
    'au_do' => 'دولار سينغافوري',
    'back' => 'رجوع',
    'up_sit_set' => 'تحديث إعدادات الموقع',
    'Site_Settings' => 'إعدادات الموقع',
    'Site_Name' => 'اسم الموقع',
    'Site_Logo' => 'شعار الموقع',
    'Site_Icon' => 'أيقونة الموقع',
    'Site_Splash' => 'دفقة',
    'Application_Status' => 'حالة التطبيق',
    'Enable' => 'مكن',
    'Disable' => 'تعطيل',
    'Application_Message' => 'رسالة التطبيق',
    'Address' => 'عنوان',
    'Copyright_Content' => 'حقوق الطبع والنشر المحتوى',
    'Site_Copyright' => 'حقوق الطبع والنشر للموقع',
    'Playstore_link' => 'رابط Playstore',
    'Appstore_Link' => 'رابط Appstore',
    'Provider_Accept_Timeout' => 'مزود قبول مهلة',
    'Provider_Search_Radius' => 'مزود البحث ',
    'SOS_Number' => 'رقم SOS',
    'Contact_Number' => 'رقم الاتصال',
    'Contact_Email' => 'تواصل بالبريد الاكتروني',
    'Social_Login' => 'تسجيل الدخول الاجتماعي',
    'App_Interval_Time' => 'وقت الفاصل الزمني للتطبيق (MS)',
    'Search_Title' => 'بحث العنوان',
    'Facebook_Link' => 'رابط الفيس بوك',
    'Twitter_Link' => 'رابط تويتر',
    'First_site_photo' => 'صورة الأولى للموقع ',
    'Text_in_first_photo' => 'النص في الصورة الأولى',
    'Second_site_photo' => 'الصورة الثانية للموقع',
    'About_Us_Big_Title' => 'من نحن عنوان كبير',
    'About_Us_small_Title' => 'من نحن عنوان صغير',
    'about_small_title' => 'حول عنوان صغير',
    'First_about_photo' => 'الصورة الأولى',
    'First_name' => 'الاسم الأول',
    'First_details' => 'التفاصيل الأولى',
    'Second_about_photo' => 'عن الصورة الثانية',
    'Second_name' => 'ألأسم الثانى',
    'Second_details' => 'التفاصيل الثانية',
    'Third_about_photo' => 'عن الصورة الثالثة',
    'Third_name' => 'الاسم الثالث',
    'Third_details' => 'التفاصيل الثالثة',
    'footer_photo' => 'صورة الفوتر',
    'Delete_Selected_Box' => 'حذف المربع المحدد',
    'Modal_title' => 'عنوان الوسائط',
    'box_photo' => 'مربع الصورة',
    'box_title' => 'مربع العنوان',
    'box_details' => 'مربع التفاصيل',
    'box_link' => 'رابط الصندوق',
    'Close' => 'غلق',
    'Save changes' => 'حفظ التغيرات',
    'about_title' => 'عن العنوان',
    'Box' => 'صندوق',
    'Add_Box' => 'إضافة مربع',

    "Transportation Types"=>"أنواع النقل",
    "ID"=>"المسلسل",
    "Name"=>"الاسم",
    "Image"=>"الصوره",
    "Capacity"=>"سعة",
    "Status"=>"الحاله",
    "Action"=>"التفاعل",
    "Edit"=>"تعديل",
    "Delete"=>"حذف",
    "New Transportation Types"=>"اضافه انواع نقل جديده",

    "Wallet"=>"محفظة نقود",
    "Transportation Name"=>"اسم النقل",
    "Transportation image"=>"صورة النقل",
    "Transportation capacity"=>"سعه النقل",
    "Close"=>"أغلق",
    "Update Transportation Type"=>"تحديث نوع النقل",
    "Create Transportation Type"=>"إنشاء نوع النقل",

    "Service Types"=>"أنواع الخدمات",
    "Service Name"=>"اسم الخدمه",
    "Base Price"=>"السعر الأساسي",
    "Base Distance"=>"قاعدة المسافة",
    "Base Waiting"=>"قاعدة الانتظار",
    "Distance Price"=>"سعر المسافة",
    "Time Price"=>"سعر الوقت",
    "Waiting Price"=>"سعر الانتظار",
    "Price Calculation"=>"حساب السعر",
    "Service Image"=>"صورة الخدمة",
    "Edit Service Type"=>"تحرير نوع الخدمة",
    "Unit Time Pricing"=>"وحدة وقت التسعير",
    "For Rental amount per hour"=>"للحصول على مبلغ الإيجار في الساعة",
    "Unit Distance Price"=>"سعر وحدة المسافة",
    "Waiting Per Minute"=>"الانتظار لكل دقيقة",
    "Pricing Logic"=>"منطق التسعير",
    "Description"=>"وصف",
    "Update Service Type"=>"تحديث نوع الخدمة",
    "New"=>"نوع الخدمة الجديد",
    "Create Service Type"=>"إنشاء نوع الخدمة",

    "Car Classes"=>"فئات السيارات",
    "New Car Class"=>"فئة السيارات الجديدة",
    "Name"=>"الاسم",
    "logo"=>"شعار",
    "Edit Car Class"=>"تحرير فئة السيارات",
    "Car Class Name"=>"اسم فئة السيارة",
    "Transportation Name"=>"اسم النقل",
    "Car_Logo"=>"شعار فئه السيارة",
    "Update Type"=>"نوع التحديث",
    "New Car Class"=>"فئة السيارات الجديدة",
    "Create Type"=>"إنشاء نوع",

    "Car Models"=>"نماذج السيارات",
    "Model Name"=>"اسم النموذج",
    "Model Production Date"=>"تاريخ الإنتاج النموذجي",
    "Transportation Type"=>"نوع النقل",
    "Service Type"=>"نوع الخدمة",
    "Car Class"=>"فئة السيارات",
    "Status"=>"الحاله",
    "Edit Car Model"=>"تحرير طراز السيارة",
    "Production Date"=>"تاريخ الإنتاج",
    "Update Car Model"=>"تحديث طراز السيارة",
    "New Car Model"=>"نموذج سيارة جديدة",
    "Create Car Model"=>"إنشاء طراز السيارة",


    "Booking ID"=>"معرف الحجز",
    "User Name"=>"اسم المستخدم",
    "Provider Name"=>"اسم السائق",
    "Date"=>"التاريخ",
    "Time"=>"الوقت",
    "Amount"=>"الكميه",
    "Payment Mode"=>"طريقة الدفع",
    "Payment Status"=>"حالة السداد",
    "Paid"=>"دفع",
    "Not Paid"=>"لا تدفع",
    "More Details"=>"المزيد من التفاصيل",
    "No results found"=>"لا توجد نتائج",


    "Request Id"=>"طلب معرف",
    "User Name"=>"اسم المستخدم",
    "Provider Name"=>"اسم السائق",
    "Scheduled Date"=>"الموعد المقرر",


    "Dispatcher"=>"المرسلات",
    "Add New Dispatcher"=>"اضافه مرسل جديد",
    "Full Name"=>"الاسم بالكامل",
    "Email"=>"البريد الإلكتروني",
    "Mobile"=>"التليفون المحمول",
    "Back"=>"رجوع",
    "Update Dispatcher"=>"تحديث المرسل",
    "Cancel"=>"ألغاء",
    "Add Dispatcher"=>"إضافة مرسل جديد",
    "Password"=>"كلمه السر",
    "Password Confirmation"=>"تاكيد كلمه السر",


    "Fleet Owners"=>"أصحاب الشركات",
    "Add New Fleet Owner"=>"أضف مالك شركة جديد",
    "Company Name"=>"اسم الشركة",
    "Company_logo"=>"شعار الشركة",
    "Update Fleet"=>"تحديث الشركة",
    "Show Provider"=>"عرض سائقين الشركة",
    "Update Fleet Owner"=>"تحديث مالك الشركة",
    "Add Fleet Owner"=>"إضافة مالك الشركة",
    //creat provider
    'Back' => 'رجوع',
    'Add_Provider' => 'إضافة سائق',
    'First_Name' => 'الاسم الاول',
    'Last_Name' => 'الاسم الاخير',
    'Email' => 'البريد الالكترونى ',
    'Password' => 'كلمة المرور',
    'Password Confirmation' => 'تأكيد كلمة المرور',
    'logo' => 'شعار السيارة',
    'Car_Front' => 'جبهة السيارة',
    'Car_Back' => 'الجزء الخلفي للسيارة',
    'Driver_Licence Front' => 'رخصة القيادة(الأمام)',
    'Driver_Licence Back' => 'رخصة القيادة(الخلف)',
    'Identity_Front' => 'صورة البطاقة(الأمام)',
    'Identity_back' => 'صورة البطاقة(الخلف)',
    'Mobile' => 'التليفون المحمول',
    'Add_Provider' => 'إضافة سائق',
    'Cancel' => 'إلغاء',
    'Re_type_Password' => 'أعد إدخال كلمة السر',
    //edite providers
    'Update_Provider' => 'تحديث بيانات السائق',
    'Provider_Name' => 'إسم السائق',
    'Document' => 'المستندات',
    //index providers
    'Providers' => 'السائقين',
    'Add_New_Provider' => 'إضافة سائق جديد',
    'Full_Name' => 'الاسم بالكامل',
    'Email' => 'البريد الإلكترونى',
    'Mobile' => 'التليفون المحمول',
    'Total_Requests' => 'اجمالى الطلبات',
    'Accepted_Requests' => 'الطلبات المقبولة',
    'Cancelled_Requests' => 'الطلبات المرفوضة',
    'Documents_Service_Type' => 'المستندات / نوع الخدمة',
    'Online' => 'عبر الانترنت',
    'Action' => 'التفاعل',
    'Yes' => 'نعم',
    'No' => 'لا',
    'N_A' => 'N/A',
    'Disable' => 'تعطيل',
    'Enable' => 'تفعيل',
    'History' => 'التاريخ',
    'Statements' => 'البيانات',
    'Edit' => 'تعديل',
    'Delete' => 'حذف',
    'Attention' => 'انتباه!',
    'All_Set' => 'كل مجموعة!',
    //details providor
    'Provider_Details' => 'تفاصيل السائق',
    'Approved' => 'وافق',
    'Not_Approved' => 'لا اوافق',
    'Activated' => 'مفعل',
    'Not_Activated' => 'غير مفعل',
    'Gender' => 'النوع',
    'Address' => 'العنوان',
    //providor statment
    'Earnings' => 'أرباح',
    'Provider_Name' => 'اسم السائق',
    'Status' => 'الحالة',
    'Total_Rides' => 'عدد الركوب',
    'Total_Earning' => 'اجمالى الارباح',
    'Commission' => 'عمولة',
    'Details' => 'تفاصيل',
    'View_by_Ride' => 'عرض بواسطة ركوب',
    'No_results_found' => 'لا توجد نتائج',

    //statment
    'Over_All_Earning' => 'على جميع الكسب: :',
    'Over_All_Commission' => 'على كل العمولة:',
    'Total_No_of_Rides' => 'إجمالى عدد الرحلات',
    'down_from_cancelled_Request' => '٪ أسفل من الطلب الملغى',
    'Revenue' => 'الإيرادات',
    'Rides' => 'الركوب',
    'Cancelled_Rides' => 'الغاء الركوب',
    'Booking_ID' => 'معرف الحجز',
    'Picked_up' => 'التقط',
    'Dropped' => 'إسقاط',
    'Request_Details' => 'طلب تفاصيل',
    'Dated_on' => 'بتاريخ',
    'Status' => 'الحالة',
    'Earned' => 'حصل',
    'View_Ride_Details' => 'عرض تفاصيل ركوب',
    'No_Details_Found ' => 'لم يتم العثور على تفاصيل',
    'No_results_found' => 'لا توجد تفاصيل',
    'from' => 'من',

    //create user
    'Add_User' => 'إضافة مستخدم',
    'Picture' => 'الصوره',

    //update user
    'Update_User' => 'تحديث المستخدم',
    'Users' => 'المستخدمين',
    'Add_New_User' => 'إضافة مستخدم جديد',
    'Rating' => 'تقييم',
    'Wallet_Amount' => 'محفظة المبلغ',
    'History' => 'التاريخ',
    'Wallet' => 'محفظة نقود',
    'Edit' => 'تعديل',
    'Delete' => 'حذف',
    'Blanace' => 'توازن',
    'Update_Wallet' => 'تحديث المحفظة',

    //User Details
    'User_Details' => 'بيانات المستخدم',
    'Wallet_Balance' => 'رصيد المحفظة',
    'Provider_Service_Type_Allocation' => 'تخصيص نوع مزود الخدمة',
    'Allocated_Services' => 'الخدمات المخصصة :  ',
    'Service_Name' => 'اسم الخدمة',
    'Service_Number' => 'رقم الخدمة',
    'Service_Model' => 'نموذج الخدمة',
    'Update' => 'تحديث',
    'Provider_Documents' => 'وثائق مزود',
    'Document_Type' => 'نوع الوثيقة',
    'Status' => 'الحالة',
    'View' => 'رأي',
    "Add Or Remove Balanace For"=>"اضافه او ازاله التوازن ل",
    "Joined_at"=>"انضم في",

    "Account Manager"=>"إدارة حساب المستخدم",
    "Add New Account Manager"=>"إضافة مدير حساب جديد",
    "Add Account Manager"=>"إضافة مدير الحساب",
    "Update Account Manager"=>"تحديث مدير الحساب",

    "Update Profile"=>"تحديث الملف",
    "Name"=>"الاسم",

    "Request ID"=>"طلب معرف",
    "User Name"=>"اسم المستخدم",
    "Provider Name"=>"اسم السائق",
    "Rating"=>"تقييم",
    "Comments"=>"التعليقات",
    "User Reviews"=>"مراجعات المستخدم",

    "Provider Reviews"=>"موفر الاستعراضات",
    "Provider Name"=>"اسم السائق",

    "Documents"=>"مستندات",
    "Document Name"=>"اسم المستند",
    "Type"=>"النوع",
    "Add New Document"=>"اضافه مستند جديد",
    "Add Document"=>"اضافه المستند",
    "Document Type"=>"نوع المستند",
    "Driver Review"=>"مراجعة السائق",
    "Vehicle Review"=>"مراجعة السيارة",
    "Update Document"=>"تحديث المستند",

    "Promocodes"=>"البرومو كود",
    "Add New Promocode"=>"إضافة برومو كود جديد",
    "Promocode"=>"البرومو كود",
    "Discount"=>"خصم",
    "Expiration"=>"انتهاء الصلاحية",
    "Used Count"=>"عدد الاستخدام",
    "Add Promocode"=>"أضف برومو كود",
    "Update Promocode"=>"تعديل البرومو كود ",

    "Map View"=>"عرض الخريطة",
    "Note:"=>": ملحوظه",

    "Service Conditions"=>"شروط الخدمة",
    "New Condition Types"=>"أنواع حالة جديدة",
    "Condition Title"=>"حالة الشرط بالغة العربية",
    "Condition Detaills"=>" تفاصيل الحالة بالغة العربية",
    "Condition Title_en"=>"حالة الشرط بالغة الإنجليزية",
    "Condition Detaills_en"=>" تفاصيل الحالة بالغة الإنجليزية",
    "Edit Service Type"=>" إضافة نوع الخدمة",
    "Add Condition"=>"إضافة شرط",

    "Help"=>"المساعده",
    "profile"=>"الملف الشخصي",
    "user_mobile"=>"رقم المستخدم المرسل إلية",
    "user_mobile_to"=>"المستخدم المرسل إلية",
    "Choose_phone"=>"اختار رقم الهاتف ",










];
?>