<?php

/*
|--------------------------------------------------------------------------
| User Authentication Routes
|--------------------------------------------------------------------------
*/

Auth::routes();
    /*
    |--------------------------------------------------------------------------
    | Provider Authentication Routes
    |--------------------------------------------------------------------------
    */

    Route::group(['prefix' => 'provider'], function () {
        Route::get('auth/facebook', 'Auth\SocialLoginController@providerToFaceBook');
        Route::get('auth/google', 'Auth\SocialLoginController@providerToGoogle');

        Route::get('/login', 'ProviderAuth\LoginController@showLoginForm');
        Route::post('/login', 'ProviderAuth\LoginController@login');
        Route::post('/logout', 'ProviderAuth\LoginController@logout');

        Route::get('/register', 'ProviderAuth\RegisterController@showRegistrationForm');
        Route::post('/register', 'ProviderAuth\RegisterController@register');

        Route::post('/password/email', 'ProviderAuth\ForgotPasswordController@sendResetLinkEmail');
        Route::post('/password/reset', 'ProviderAuth\ResetPasswordController@reset');
        Route::get('/password/reset', 'ProviderAuth\ForgotPasswordController@showLinkRequestForm');
        Route::get('/password/reset/{token}', 'ProviderAuth\ResetPasswordController@showResetForm');

    });
    /*
    |--------------------------------------------------------------------------
    | Admin Authentication Routes
    |--------------------------------------------------------------------------
    */
    Route::group(['prefix' => 'admin'], function () {
        Route::get('/login', 'AdminAuth\LoginController@showLoginForm');
        Route::post('/login', 'AdminAuth\LoginController@login');
        Route::post('/logout', 'AdminAuth\LoginController@logout');
        Route::post('/password/email', 'AdminAuth\ForgotPasswordController@sendResetLinkEmail');
        Route::post('/password/reset', 'AdminAuth\ResetPasswordController@reset');
        Route::get('/password/reset', 'AdminAuth\ForgotPasswordController@showLinkRequestForm');
        Route::get('/password/reset/{token}', 'AdminAuth\ResetPasswordController@showResetForm');
    });
/*
    |--------------------------------------------------------------------------
    | User Routes
    |--------------------------------------------------------------------------
    */


    Route::get('/dashboard', 'HomeController@index');
    Route::get('/services/{id}', 'Resource\ServiceResource@frontend')->name('frontend.service');
    Route::get('/about',function(){
         return view('about');
    });
    // user profiles
    Route::get('/profile', 'HomeController@profile');
    Route::get('/edit/profile', 'HomeController@edit_profile');
    Route::post('/profile', 'HomeController@update_profile');
    // update password
    Route::get('/change/password', 'HomeController@change_password');
    Route::post('/change/password', 'HomeController@update_password');
    // ride
    Route::get('/confirm/ride', 'RideController@confirm_ride');
    Route::post('/create/ride', 'RideController@create_ride');
    Route::post('/cancel/ride', 'RideController@cancel_ride');
    Route::get('/onride', 'RideController@onride');
    Route::post('/payment', 'PaymentController@payment');
    Route::post('/rate', 'RideController@rate');
    // status check
    Route::get('/status', 'RideController@status');
    // trips
    Route::get('/trips', 'HomeController@trips');
    Route::get('/upcoming/trips', 'HomeController@upcoming_trips');
    // wallet
    Route::get('/wallet', 'HomeController@wallet');
    Route::post('/add/money', 'PaymentController@add_money');
    // payment
    Route::get('/payment', 'HomeController@payment');
    // card
    Route::resource('card', 'Resource\CardResource');
    // promotions
    Route::get('/promotions', 'HomeController@promotions_index')->name('promocodes.index');
    Route::post('/promotions', 'HomeController@promotions_store')->name('promocodes.store');


    Route::get('auth/facebook', 'Auth\SocialLoginController@redirectToFaceBook');
    Route::get('auth/facebook/callback', 'Auth\SocialLoginController@handleFacebookCallback');
    Route::get('auth/google', 'Auth\SocialLoginController@redirectToGoogle');
    Route::get('auth/google/callback', 'Auth\SocialLoginController@handleGoogleCallback');
    Route::post('account/kit', 'Auth\SocialLoginController@account_kit')->name('account.kit');

    /*
    |--------------------------------------------------------------------------
    | Web Routes
    |--------------------------------------------------------------------------
    */

    Route::get('/privacy', function () {
        $page = 'page_privacy';
        $title = 'سياسة الخصوصية';
        return view('static', compact('page', 'title'));
    });

    Route::get('/condition', function () {
        $page = 'privacy';
        $title = ' شروط الخدمة';
        return view('static2', compact('page', 'title'));
    });






    Route::get('/{lang}',function ($lang){
        if(in_array($lang,['ar','en'])){
            if (auth()->user()){
                $user=auth()->user();
                $user->lang=$lang;
                $user->save();
            }
            else{
                if(session()->has('lang')){
                    session()->forget('lang');
                }
                session()->put('lang',$lang);
            }
        }

        else{
            if (auth()->user()){
                $user=auth()->user();
                $user->lang='ar';
                $user->save();
            }
            else{
                if(session()->has('lang')){
                    session()->forget('lang');
                }
                session()->put('lang','ar');
            }
        }
        return back();
    });
    Route::group(['Middlware' => 'setlanguage'] ,  function  () {
    Route::get('/', function () {
        return view('index');
    });

}); //End Middlware











