<?php

use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::post('/user/signup' , 'UserApiController@signup');
Route::post('/user/logout' , 'UserApiController@logout');
Route::post('/auth/facebook', 'Auth\SocialLoginController@facebookViaAPI');
Route::post('/auth/google', 'Auth\SocialLoginController@googleViaAPI');
Route::post('/oauth/token' , 'UserApiController@authenticate');
Route::post('/user/refresh' , 'UserApiController@refresh');
Route::get('/user/account_ext' , 'UserApiController@checkAccount');
Route::post('/user/forgot/password','UserApiController@forgot_password');
Route::post('/user/reset/password','UserApiController@reset_password');
Route::get('/user/app_details' , 'UserApiController@App_Details');
Route::get('show-order', 'UserApiController@showOrder');

Route::group(['middleware' => ['auth:api']], function () {
    // user profile
    Route::get('/user/estimated/fare' , 'UserApiController@estimated_fare');
    Route::post('/user/app_destroy' , 'UserApiController@Destroy_App');



    Route::get('/user/details' , 'UserApiController@details');
	Route::post('/user/change/password' , 'UserApiController@change_password');

	Route::post('/user/update/location' , 'UserApiController@update_location');


	Route::post('/user/update/profile' , 'UserApiController@update_profile');

	// services

	Route::get('/user/services' , 'UserApiController@services');

	// provider

	Route::post('/user/rate/provider' , 'UserApiController@rate_provider');

	// request

	Route::post('/user/send/request' , 'UserApiController@send_request');

	Route::post('/user/cancel/request' , 'UserApiController@cancel_request');

	Route::get('/user/request/check' , 'UserApiController@request_status_check');
	Route::post('/user/show/providers' , 'UserApiController@show_providers');
//	Route::post('/provider-all' , 'UserApiController@show_providers');

	// history

	Route::get('/user/trips' , 'UserApiController@trips');
	Route::get('/user/upcoming/trips' , 'UserApiController@upcoming_trips');

	Route::get('/user/trip/details' , 'UserApiController@trip_details');
	Route::get('/user/upcoming/trip/details' , 'UserApiController@upcoming_trip_details');

	// payment

	Route::post('/user/payment' , 'PaymentController@payment');

	Route::post('/user/add/money' , 'PaymentController@add_money');

	// estimated

	Route::get('/user/estimated/fare' , 'UserApiController@estimated_fare');
	Route::get('/user/test' , 'UserApiController@test');

	// help

	Route::get('/user/help' , 'UserApiController@help_details');

	// promocode

	Route::get('/user/promocodes' , 'UserApiController@promocodes');

	Route::post('/user/promocode/add' , 'UserApiController@add_promocode');

	// card payment

    Route::resource('/user/card', 'Resource\CardResource');



    Route::get('/providerAll' , 'UserApiController@providerAll');

});
?>